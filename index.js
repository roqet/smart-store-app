import moment from 'moment';
import { AppRegistry } from 'react-native';

import { i18n } from '@common/services';
import 'moment/locale/uk';
import { App } from '@/app.component';
import { ua } from '@/locale';

moment.locale('uk');

// eslint-disable-next-line no-console
console.disableYellowBox = true;

i18n.init({ defaultLang: 'ua', translations: { ua } });

AppRegistry.registerComponent('smartstore', () => App);
