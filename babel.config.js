module.exports = {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [
        [
            require.resolve('babel-plugin-module-resolver'),
            {
                root: ['./src'],
                alias: {
                    '@': './src',
                    '@common': './src/common/',
                    '@theme': './src/theme',
                    '@assets': './src/assets/'
                }
            }
        ]
    ]
};
