import React from 'react';
import { Root } from 'native-base';
import { Provider, connect } from 'react-redux';
import { Router } from 'react-native-router-flux';
import EStyleSheet from 'react-native-extended-stylesheet';
import { Theme } from '@theme/theme';

import { store } from './store/app.store';
import { Scenes } from './app.scenes';
import { ScreensContainer } from '@/screens/screens.container';

EStyleSheet.build({
    $white: '#ffffff',
    $primary: '#FFED00',
    $black: '#000000',
    $dustyGray: '#979797',
    $tundora: '#434343',
    $doveGray: '#6C6C6C',
    $gainsboro: '#e5e5e5'
});

const ReduxRouter = connect()(Router);

export function App(): JSX.Element {
    return (
        <Theme>
            <Root>
                <Provider store={store}>
                    <ScreensContainer>
                        <ReduxRouter scenes={Scenes} />
                    </ScreensContainer>
                </Provider>
            </Root>
        </Theme>
    );
}
