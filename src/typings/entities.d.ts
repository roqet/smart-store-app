declare namespace Store {
    interface Geometry {
        latitude: string;
        longitude: string;
    }

    interface Promotion {
        id: number;
        title: string;
        type: 'SALES' | 'NEWS';
        content: string;
        image: Partial<CloudinaryImage>;
        createdAt: number;
    }

    interface CloudinaryImage {
        bytes: number;
        created_at: string;
        etag: string;
        format: string;
        height: number;
        original_filename: string;
        placeholder: boolean;
        public_id: string;
        resource_type: string;
        secure_url: string;
        signature: string;
        tags: [string];
        type: string;
        url: string;
        version: number;
        width: number;
    }

    interface City {
        id: number;
        name: string;
        geometry: Geometry;
    }

    interface Shop {
        id: number;
        dayOfDelivery: number;
        deliveryLux: number;
        prices: {
            deliveryPrices: number[];
            luxuryPrices: number[];
        };
        workingSchedule: {
            dayNum: number;
            start: string;
            end: string;
            isClosed: boolean;
        }[];
        address: {
            id: string;
            name: string;
            cityId: number;
        };
        geometry: Geometry;
    }

    interface BasePage {
        type: string;
        content: string;
    }

    interface Contacts extends BasePage {
        meta: {
            facebook: string;
            telegram: string;
            instagram: string;
            info: string;
            hr: string;
            tel: string;
        };
    }

    type About = BasePage;

    type Sex = 0 | 1;

    interface User {
        id: number;
        phoneNumber: string;
        firstName: string;
        lastName: string;
        dateOfBirth: number;
        sex: Sex;
        email: string;
        currentLevel: number;
        bonuses: number;
        levelPercentage: number;
        cardNumber: string;
    }
}
