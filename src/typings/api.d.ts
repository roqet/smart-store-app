declare namespace Store.Api {
    interface PaginationParams {
        page: number;
        count: number;
    }

    interface PagedResponse<T> {
        total: number;
        records: T[];
    }
}
