import { connect } from 'react-redux';
import { Cities, CitiesProps } from './cities.component';
import { ShopsSelectors, ShopsActions } from '@/store/shops';

const mapStateToProps = (state: AppState): Partial<CitiesProps> => ({
    cities: ShopsSelectors.cities(state),
    cityId: ShopsSelectors.cityId(state)
});

const mapDispatchToProps = (dispatch): Partial<CitiesProps> => ({
    update: (id: number): void => dispatch(new ShopsActions.UpdateShops(id))
});

export const CitiesContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Cities as any);
