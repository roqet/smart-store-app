import React from 'react';
import { FlatList } from 'react-native';
import { Container, Header, Left, Button, Body, Right, Content, ListItem, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { Theme } from '@theme/theme';
import { i18n } from '@common/services';
import { Icon, Separator, ListHeader } from '@common/components';
import { Icons } from '@common/constants';
import { styles } from './cities.styles';

const checkmark: JSX.Element = (
    <Right>
        <Icon name={Icons.checkmark} />
    </Right>
);

export function Cities({ cities, cityId, update }: CitiesProps): JSX.Element {
    return (
        <Theme>
            <Container>
                <Header noShadow transparent>
                    <Left style={styles.left}>
                        <Button transparent onPress={(): void => Actions.pop()}>
                            <Icon name={Icons.x} />
                        </Button>
                    </Left>
                    <Body />
                </Header>
                <Content>
                    <FlatList
                        data={cities}
                        style={styles.list}
                        ItemSeparatorComponent={Separator}
                        keyExtractor={(item): string => item.id.toString()}
                        renderItem={({ item: { name, id } }): JSX.Element => (
                            <ListItem
                                noBorder
                                style={styles.item}
                                onPress={(): void => {
                                    if (id !== cityId) update(id);
                                    Actions.pop();
                                }}
                            >
                                <Left>
                                    <Text>{name}</Text>
                                </Left>
                                {id === cityId ? checkmark : null}
                            </ListItem>
                        )}
                        ListHeaderComponent={<ListHeader title={i18n.t('buttons.chooseCity')} />}
                    />
                </Content>
            </Container>
        </Theme>
    );
}

export interface CitiesProps {
    cities: Store.City[];
    cityId: number;
    update: (id: number) => void;
}
