import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    list: {
        paddingLeft: 4,
        paddingRight: 4
    },
    item: {
        minHeight: 58
    }
});
