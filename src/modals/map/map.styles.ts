import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    close: {
        flex: 0
    },
    header: {
        flex: 1,
        paddingLeft: 8
    },
    container: {
        flex: 1
    },
    map: {
        flex: 1,
        borderWidth: 1,
        borderColor: '$dustyGray',
        marginTop: 8
    },
    address: {
        fontSize: 18,
        lineHeight: 22
    },
    content: {
        flex: 1
    },
    icon: {
        color: '$primary',
        fontSize: 50,
        textShadowColor: 'rgba(0, 0, 0, 1)',
        textShadowOffset: { width: 0, height: 0 },
        textShadowRadius: 1,
        padding: 1
    }
});
