import { connect } from 'react-redux';
import { Map, MapProps } from './map.component';

const mapStateToProps = (state: AppState, props: MapProps): Partial<MapProps> => ({
    shop: props.shop
});

const mapDispatchToProps = (dispatch): Partial<MapProps> => ({});

export const MapContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Map as any);
