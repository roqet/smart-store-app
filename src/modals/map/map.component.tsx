import get from 'lodash/get';
import React from 'react';
import { Container, Header, View, Button, Body, Left, Text } from 'native-base';
import MapView, { Marker } from 'react-native-maps';
import { Actions } from 'react-native-router-flux';
import { Theme } from '@theme/theme';
import { Icon } from '@common/components';
import { Icons } from '@common/constants';
import { styles } from './map.styles';

export function Map({ shop }: MapProps): JSX.Element {
    return (
        <Theme>
            <Container>
                <Header noShadow transparent>
                    <Left style={styles.close}>
                        <Button transparent onPress={(): void => Actions.pop()}>
                            <Icon name={Icons.x} />
                        </Button>
                    </Left>
                    <Body style={styles.header}>
                        <Text style={styles.address}>{get(shop, 'address.name')}</Text>
                    </Body>
                </Header>
                <View style={styles.container}>
                    <View style={styles.content}>
                        <MapView
                            style={styles.map}
                            initialRegion={{
                                latitude: parseFloat(get(shop, 'geometry.latitude')),
                                longitude: parseFloat(get(shop, 'geometry.longitude')),
                                latitudeDelta: 0.01,
                                longitudeDelta: 0.01
                            }}
                        >
                            <Marker
                                coordinate={{
                                    latitude: parseFloat(get(shop, 'geometry.latitude')),
                                    longitude: parseFloat(get(shop, 'geometry.longitude'))
                                }}
                            >
                                <Icon name={Icons.marker} style={styles.icon} />
                            </Marker>
                        </MapView>
                    </View>
                </View>
            </Container>
        </Theme>
    );
}

export interface MapProps {
    shop: Store.Shop;
}
