import React from 'react';
import { View } from 'native-base';
import { styles } from './separator.styles';

export function LevelSeparator(): JSX.Element {
    return <View style={styles.separator} />;
}
