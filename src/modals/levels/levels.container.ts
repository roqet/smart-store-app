import { connect } from 'react-redux';
import { Levels, LevelsProps } from './levels.component';

const mapStateToProps = (state: AppState, props: LevelsProps): Partial<LevelsProps> => ({
    gender: props.gender
});

export const LevelsContainer = connect(
    mapStateToProps,
    null
)(Levels as any);
