import React from 'react';
import { FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Body, Button, View, Left } from 'native-base';
import { Theme } from '@theme/theme';
import { i18n } from '@common/services';
import { Icons, Level } from '@common/constants';
import { Icon, ListHeader } from '@common/components';
import { LevelItem } from './item/item.component';
import { LevelSeparator } from './separator/separator.component';
import { styles } from './levels.styles';

export function Levels({ gender = 1 }: LevelsProps): JSX.Element {
    return (
        <Theme>
            <Container>
                <Header noShadow transparent>
                    <Left>
                        <Button transparent onPress={(): void => Actions.pop()}>
                            <Icon name={Icons.x} />
                        </Button>
                    </Left>
                    <Body />
                </Header>
                <View style={styles.container}>
                    <FlatList
                        data={Object.keys(Level)}
                        renderItem={({ item }): JSX.Element => (
                            <LevelItem gender={gender} level={Level[item]} />
                        )}
                        ListHeaderComponent={
                            <ListHeader
                                title={i18n.t('levels.levelsAndRanks')}
                                note={i18n.t('levels.levelsAndRanksInfo')}
                            />
                        }
                        ListFooterComponent={<LevelSeparator />}
                        ItemSeparatorComponent={(): JSX.Element => <LevelSeparator />}
                    />
                </View>
            </Container>
        </Theme>
    );
}

export interface LevelsProps {
    gender: Store.Sex;
}
