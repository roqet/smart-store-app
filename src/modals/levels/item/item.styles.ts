import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    item: {
        paddingLeft: 48,
        paddingTop: 0,
        paddingBottom: 0,
        marginLeft: 0,
        alignItems: 'flex-start'
    },
    left: {
        flex: 0,
        width: 76,
        marginTop: 6,
        position: 'relative'
    },
    image: {
        width: 76,
        height: 76
    },
    circle: {
        width: 34,
        height: 34,
        backgroundColor: '$primary',
        position: 'absolute',
        right: -8,
        top: -2,
        borderRadius: 17,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    discount: {
        fontSize: 18,
        fontWeight: 'bold'
    },
    percent: {
        position: 'absolute',
        right: 2,
        top: 3,
        fontSize: 8
    },
    body: {
        flex: 1,
        paddingLeft: 37
    },
    lvl: {
        fontSize: 14,
        marginBottom: 3
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 3
    },
    note: {
        fontSize: 14,
        lineHeight: 20,
        color: '$doveGray'
    }
});
