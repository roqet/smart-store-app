import React from 'react';
import { Image } from 'react-native';
import { ListItem, Left, Body, Text, View } from 'native-base';
import { Level, MaleAvatars, FemaleAvatars, LevelDiscountMap, Icons } from '@common/constants';
import { i18n } from '@common/services';
import { Icon } from '@common/components';
import { styles } from './item.styles';

interface LevelItemProps {
    gender: Store.Sex;
    level: Level;
}

const avatars: Record<Store.Sex, any> = {
    0: MaleAvatars,
    1: FemaleAvatars
};

export function LevelItem({ gender, level }: LevelItemProps): JSX.Element {
    return (
        <ListItem noBorder style={styles.item}>
            <Left style={styles.left}>
                <Image source={avatars[gender][level]} style={styles.image} />
                <View style={styles.circle}>
                    <Text style={styles.discount}>{LevelDiscountMap[level]}</Text>
                    <Icon name={Icons.percent} style={styles.percent} />
                </View>
            </Left>
            <Body style={styles.body}>
                <Text style={styles.lvl}>{i18n.t(`levels.lvl.${level}`)}</Text>
                <Text style={styles.title}>{i18n.t(`levels.title.${level}`)}</Text>
                <Text style={styles.note}>{i18n.t(`levels.note.${level}`)}</Text>
            </Body>
        </ListItem>
    );
}
