import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    container: {
        backgroundColor: '$primary'
    },
    content: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center'
    },
    card: {
        borderRadius: 25,
        backgroundColor: '$black',
        width: 548,
        height: 336
    },
    beImg: {
        width: 25,
        height: 16,
        position: 'absolute',
        left: 84,
        top: 67
    },
    smartImg: {
        width: 106,
        height: 22,
        position: 'absolute',
        left: 106,
        top: 62
    },
    html: {
        width: 180,
        position: 'absolute',
        left: 79,
        top: 168
    },
    barcode: {
        position: 'absolute',
        left: 273,
        top: 140,
        paddingVertical: 6,
        paddingHorizontal: 6,
        backgroundColor: '$white'
    },
    close: {
        position: 'absolute',
        right: 10,
        top: 5
    }
});

export const html = {
    p: {
        color: '#ffffff',
        fontSize: 36,
        lineHeight: 40,
        fontWeight: 'bold'
    },
    span: {
        color: '#FFED00'
    }
};
