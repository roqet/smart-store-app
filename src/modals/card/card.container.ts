import { connect } from 'react-redux';
import { Card, CardProps } from './card.component';
import { RootSelectors } from '@/store/root';

const mapStateToProps = (state: AppState): Partial<CardProps> => ({
    user: RootSelectors.user(state)
});

export const CardContainer = connect(mapStateToProps)(Card as any);
