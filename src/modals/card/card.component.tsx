/* eslint-disable @typescript-eslint/no-var-requires */
import React, { useEffect, useState, EffectCallback, useCallback } from 'react';
import { Image, ViewStyle } from 'react-native';
import Orientation, { OrientationType } from 'react-native-orientation-locker';
import Barcode from 'react-native-barcode-builder';
import { Container, View, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { i18n } from '@common/services';
import { Icon, Html } from '@common/components';
import { Icons } from '@common/constants';
import { styles, html } from './card.styles';

type ScreenOrientation =
    | 'LANDSCAPE-LEFT'
    | 'LANDSCAPE-RIGHT'
    | 'PORTRAIT'
    | 'PORTRAITUPSIDEDOWN'
    | 'UNKNOWN';

const beImg = require('@/assets/be.png');
const smartImg = require('@/assets/smart.png');

const getRotation = (revers: boolean): ViewStyle => {
    const width = 548;
    const height = 336;
    const offset = width / 2 - height / 2 - 20;
    return {
        transform: [
            { rotate: `${revers ? '-' : ''}90deg` },
            { translateX: 0 },
            { translateY: revers ? -offset : offset }
        ]
    };
};

export function Card({ user: { cardNumber } }: CardProps): JSX.Element {
    const [rotation, setRotation] = useState<ViewStyle>({});

    const orientationListener = useCallback((orientation: OrientationType): void => {
        console.log(orientation);
        if (orientation === 'LANDSCAPE-LEFT') {
            setRotation(getRotation(false));
        } else {
            setRotation(getRotation(true));
        }
    }, []);

    useEffect((): EffectCallback => {
        return (): void => {};
    }, []);

    useEffect((): void => {
        Orientation.getDeviceOrientation((orientation: OrientationType): void => {
            orientationListener(orientation);
        });
    }, [orientationListener]);

    useEffect((): EffectCallback => {
        Orientation.addDeviceOrientationListener(orientationListener);
        return (): void => {
            Orientation.lockToPortrait();
            Orientation.removeDeviceOrientationListener(orientationListener);
        };
    }, [orientationListener]);

    return (
        <Container style={styles.container}>
            <View style={styles.content}>
                <View style={{ ...styles.card, ...rotation }}>
                    <Button style={styles.close} transparent onPress={(): void => Actions.pop()}>
                        <Icon name={Icons.x} style={{ color: '#fff' }} />
                    </Button>
                    <Image source={beImg} style={styles.beImg} />
                    <Image source={smartImg} style={styles.smartImg} />
                    <View style={styles.html}>
                        <Html
                            styles={html}
                            content={`<p>${i18n.t('labels.buyThisOneMakeMoney')}</p>`}
                        />
                    </View>
                    <View style={styles.barcode}>
                        <Barcode
                            value={cardNumber}
                            background='#ffffff'
                            lineColor='#000000'
                            height={100}
                            width={2}
                            format='EAN13'
                            flat
                        />
                    </View>
                </View>
            </View>
        </Container>
    );
}

export interface CardProps {
    user: Store.User;
}
