import pages from './pages';
import forms from './forms';
import buttons from './buttons';
import levels from './levels';
import labels from './labels';
import notes from './notes';
import errors from './errors';

export const ua = {
    pages,
    forms,
    buttons,
    levels,
    labels,
    notes,
    errors
};
