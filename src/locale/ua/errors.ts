export default {
    common: 'Щось пішло не так. Будь-ласка, спробуйте пізніше.',
    pleaseWait1MinBeforeSecondLogin: 'Будь ласка, зачекайте 1 хвилину перед наступною спробою.'
};
