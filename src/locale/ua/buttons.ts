export default {
    chooseCity: 'Оберіть місто',
    shopOnMap: 'Магазин на мапі',
    proceed: 'Продовжити',
    complete: 'Завершити',
    iDoNotHaveCard: 'У мене ще немає карти лояльності',
    iHaveCard: 'У мене вже є карта лояльності',
    customerCard: 'Карта клієнта'
};
