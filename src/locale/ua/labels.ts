export default {
    welcome: 'Ласкаво просимо',
    cashback: '{{value}} грн',
    level: '{{level}} рівень',
    fullName: '{{firstName}} {{lastName}}',
    buyThisOneMakeMoney: 'Купуй та <span>заробляй</span>'
};
