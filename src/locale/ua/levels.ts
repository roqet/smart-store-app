export default {
    levelsAndRanks: 'Рівні та ранги',
    levelsAndRanksInfo:
        'купуючи речі в мережі магазинів SMART STORE, ви накопичуєте кешбек та прокачуєте ваш рівень. Вище рівень - більше кешбек.',
    title: {
        newcomer: 'новачок',
        wiseman: 'розумник',
        amateur: 'любитель',
        enthusiast: 'ентузіаст',
        advanced: 'просунутий',
        shopaholic: 'шопоголік',
        profі: 'профі',
        onioman: 'оніоман',
        master: 'магістр',
        sensei: 'сенсей',
        guru: 'гуру'
    },
    note: {
        newcomer: 'welcome to the club, buddy!',
        wiseman: 'знає, чому тут вигідно',
        amateur: 'відвідування магазину вже не звичка, а любов',
        enthusiast: 'завжди з гарним настроєм',
        advanced: 'заходжу першим в магазин',
        shopaholic: 'вже не можу без цього',
        profі: 'думаєш це як робота? Ні це задоволення',
        onioman: 'ніби одружилися',
        master: 'майже пізнав дзен',
        sensei: 'пізнав дзен',
        guru: 'навчу як пізнавати дзен'
    },
    lvl: {
        newcomer: '1 рівень',
        wiseman: '2 рівень',
        amateur: '3 рівень',
        enthusiast: '4 рівень',
        advanced: '5 рівень',
        shopaholic: '6 рівень',
        profі: '7 рівень',
        onioman: '8 рівень',
        master: '9 рівень',
        sensei: '10 рівень',
        guru: '11 рівень'
    }
};
