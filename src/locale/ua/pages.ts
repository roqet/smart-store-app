export default {
    home: 'Головна',
    shops: 'Магазини',
    promotions: 'Акції',
    newsSlashPromotions: 'Новини | Акції',
    aboutUs: 'Про нас',
    contacts: 'Контакти',
    createShop: 'Створити магазин',
    editShop: 'Редагувати магазин',
    createNewsSlashPromotion: 'Створити Новину / Акцію',
    editNewsSlashPromotion: 'Редагувати Новину / Акцію',
    aboutUsColonEdit: 'Про нас: Редагувати',
    contactsColonEdit: 'Контакти: Редагувати',
    cities: 'Міста',
    addCity: 'Додати місто',
    shopsSlashCities: 'Магазини / Міста',
    personalOffice: 'Особистий кабінет',
    notifications: 'Повідомлення'
};
