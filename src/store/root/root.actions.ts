import { Action } from 'redux';

export enum Types {
    INIT = '[ROOT] INIT',
    INITIALIZED = '[ROOT] INITIALIZED',
    HTTP_ERROR = '[ROOT] HTTP_ERROR'
}

export class Init implements Action {
    public readonly type = Types.INIT;
}

export class Initialized implements Action {
    public readonly type = Types.INITIALIZED;
    public constructor(public user: Store.User) {}
}

export class HttpError implements Action {
    public readonly type = Types.HTTP_ERROR;
    public constructor(public errors: string[]) {}
}

export type All = Types & Init & Initialized & HttpError;
