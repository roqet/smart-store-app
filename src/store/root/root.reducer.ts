import { Types, All } from './root.actions';

export interface RootState {
    user: Store.User;
    initialized: boolean;
    errors: string[];
}

export const rootInitialState: RootState = {
    user: null,
    initialized: false,
    errors: []
};

export function rootReducer(state: RootState = rootInitialState, action: All): RootState {
    switch (action.type) {
        case Types.INIT:
            return { ...state, initialized: false };
        case Types.INITIALIZED:
            return {
                ...state,
                initialized: true,
                user: action.user
            };
        case Types.HTTP_ERROR:
            return { ...state, errors: action.errors };
        default:
            return state;
    }
}
