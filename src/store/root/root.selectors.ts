import { RootState } from './root.reducer';
import { appSelectors } from '../app.selectors';

const select = appSelectors.fromRootState;

const Selectors = {
    errors: ({ errors }: RootState): string[] => errors,
    user: ({ user }: RootState): Store.User => user,
    initialized: ({ initialized }: RootState): boolean => initialized
};

export const RootSelectors = {
    errors: select(Selectors.errors),
    user: select(Selectors.user),
    initialized: select(Selectors.initialized)
};
