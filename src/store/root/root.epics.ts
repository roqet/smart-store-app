import { Observable, of } from 'rxjs';
import { Action } from 'redux';
import { ofType, combineEpics, ActionsObservable } from 'redux-observable';
import { map, flatMap, catchError } from 'rxjs/operators';
import { SmartApi } from '@common/api';
import { Initialized, Types } from './root.actions';

const getUser = (): Observable<Action> => {
    return SmartApi.user.current().pipe(
        map(
            (user): Initialized => {
                return new Initialized(user.firstName ? user : null);
            }
        ),
        catchError(
            (): Observable<Action> => {
                return of(new Initialized(null));
            }
        )
    );
};

const initEpic = (action$: ActionsObservable<Action>): Observable<Action> => {
    return action$.pipe(
        ofType(Types.INIT),
        flatMap((): Observable<Action> => getUser())
    );
};

export const rootEpics = combineEpics(initEpic);
