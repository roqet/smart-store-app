import { Observable, of } from 'rxjs';
import { Action } from 'redux';
import { ofType, combineEpics, ActionsObservable, StateObservable } from 'redux-observable';
import { map, flatMap, catchError } from 'rxjs/operators';
import { SmartApi } from '@common/api';
import { ShopsSelectors } from './shops.selectors';
import * as ShopsActions from './shops.actions';
import * as RootActions from '../root/root.actions';

const loadCities = (): Observable<Action> => {
    return SmartApi.city.all({ id: true, name: true }).pipe(
        map(
            (cities): ShopsActions.CitiesLoaded => {
                return new ShopsActions.CitiesLoaded(cities);
            }
        ),
        catchError(
            ({ errors }): Observable<Action> => {
                return of(new RootActions.HttpError(errors), new ShopsActions.Loading(false));
            }
        )
    );
};

const initializeEpic = (action$: ActionsObservable<Action>): Observable<Action> => {
    return action$.pipe(
        ofType(ShopsActions.Types.INITIALIZE),
        flatMap((): Observable<Action> => loadCities())
    );
};

const updateShopsEpic = (
    action$: ActionsObservable<Action>,
    state$: StateObservable<AppState>
): Observable<Action> => {
    return action$.pipe(
        ofType(ShopsActions.Types.UPDATE_SHOPS),
        map((): number => ShopsSelectors.cityId(state$.value)),
        flatMap(
            (id): Observable<Store.Shop[]> => {
                return SmartApi.shop.all(id, {
                    id: true,
                    dayOfDelivery: true,
                    deliveryLux: true,
                    address: {
                        name: true
                    }
                });
            }
        ),
        map(
            (shops): ShopsActions.ShopsLoaded => {
                return new ShopsActions.ShopsLoaded(shops);
            }
        )
    );
};

export const shopsEpics = combineEpics(initializeEpic, updateShopsEpic);
