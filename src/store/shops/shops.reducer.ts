import merge from 'lodash/merge';
import { Types, All } from './shops.actions';

export interface ShopsState {
    initialized: boolean;
    loading: boolean;
    cities: Store.City[];
    shops: Store.Shop[];
    cityId: number;
}

export const shopsInitialState: ShopsState = {
    initialized: false,
    loading: false,
    cities: [],
    shops: [],
    cityId: 0
};

export function shopsReducer(state: ShopsState = shopsInitialState, action: All): ShopsState {
    switch (action.type) {
        case Types.LOADING:
            return { ...state, loading: action.loading };
        case Types.INITIALIZE:
            return { ...state, loading: true };
        case Types.CITIES_LOADED:
            return merge({}, state, { loading: false, initialized: true, cities: action.cities });
        case Types.UPDATE_SHOPS:
            return merge({}, state, { loading: true, cityId: action.cityId });
        case Types.SHOPS_LOADED:
            return { ...state, loading: false, shops: action.shops };
        default:
            return state;
    }
}
