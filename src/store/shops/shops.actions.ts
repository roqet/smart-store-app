import { Action } from 'redux';

export enum Types {
    LOADING = '[SHOPS] LOADING',
    INITIALIZE = '[SHOPS] INITIALIZE',
    CITIES_LOADED = '[SHOPS] CITIES_LOADED',
    UPDATE_SHOPS = '[SHOPS] UPDATE_SHOPS',
    SHOPS_LOADED = '[SHOPS] SHOPS_LOADED'
}

export class Loading implements Action {
    public readonly type = Types.LOADING;
    public constructor(public loading: boolean) {}
}

export class Initialize implements Action {
    public readonly type = Types.INITIALIZE;
}

export class UpdateShops implements Action {
    public readonly type = Types.UPDATE_SHOPS;
    public constructor(public cityId: number) {}
}

export class CitiesLoaded implements Action {
    public readonly type = Types.CITIES_LOADED;
    public constructor(public cities: Store.City[]) {}
}

export class ShopsLoaded implements Action {
    public readonly type = Types.SHOPS_LOADED;
    public constructor(public shops: Store.Shop[]) {}
}

export type All = Types & Loading & Initialize & CitiesLoaded & UpdateShops & ShopsLoaded;
