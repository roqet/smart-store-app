import { ShopsState } from './shops.reducer';
import { appSelectors } from '../app.selectors';

const Selectors = {
    initialized: (state: ShopsState): boolean => state.initialized,
    loading: (state: ShopsState): boolean => state.loading,
    cities: (state: ShopsState): Store.City[] => state.cities,
    shops: (state: ShopsState): Store.Shop[] => state.shops,
    cityId: (state: ShopsState): number => state.cityId
};

export const ShopsSelectors = {
    initialized: appSelectors.fromShopsState(Selectors.initialized),
    loading: appSelectors.fromShopsState(Selectors.loading),
    cities: appSelectors.fromShopsState(Selectors.cities),
    shops: appSelectors.fromShopsState(Selectors.shops),
    cityId: appSelectors.fromShopsState(Selectors.cityId)
};
