import { combineReducers, createStore, applyMiddleware, AnyAction, Dispatch } from 'redux';
import { createEpicMiddleware } from 'redux-observable';
import { composeWithDevTools } from 'redux-devtools-extension';

import { PromotionsState, promotionsReducer } from './promotions/promotions.reducer';
import { PromotionState, promotionReducer } from './promotion/promotion.reducer';
import { ContactsState, contactsReducer } from './contacts/contacts.reducer';
import { AboutState, aboutReducer } from './about/about.reducer';
import { ShopsState, shopsReducer } from './shops/shops.reducer';
import { ShopState, shopReducer } from './shop/shop.reducer';
import { RootState, rootReducer } from './root/root.reducer';
import { AuthState, authReducer } from './auth/auth.reducer';
import { appEpic } from './app.epic';

declare global {
    interface AppState {
        promotions: PromotionsState;
        promotion: PromotionState;
        contacts: ContactsState;
        about: AboutState;
        shops: ShopsState;
        shop: ShopState;
        root: RootState;
        auth: AuthState;
    }
}

type AnyActionMap = (action: AnyAction) => AnyAction;
type AnyActionDispatcher = (next: Dispatch<AnyAction>) => AnyActionMap;

const appReducer = combineReducers<AppState>({
    promotions: promotionsReducer,
    promotion: promotionReducer,
    contacts: contactsReducer,
    about: aboutReducer,
    shops: shopsReducer,
    shop: shopReducer,
    root: rootReducer,
    auth: authReducer
});

const epicMiddleware = createEpicMiddleware();

const middlewares = composeWithDevTools(
    applyMiddleware((): AnyActionDispatcher => {
        return (next: Dispatch<AnyAction>): AnyActionMap => {
            return (action: AnyAction): AnyAction => next({ ...action });
        };
    }, epicMiddleware)
);

const store = createStore(appReducer, middlewares);

epicMiddleware.run(appEpic);

export { store };
