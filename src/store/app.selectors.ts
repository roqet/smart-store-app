import { createSelector, Selector, OutputSelector } from 'reselect';
import { PromotionsState } from './promotions/promotions.reducer';
import { PromotionState } from './promotion/promotion.reducer';
import { ContactsState } from './contacts/contacts.reducer';
import { AboutState } from './about/about.reducer';
import { ShopsState } from './shops/shops.reducer';
import { ShopState } from './shop/shop.reducer';
import { RootState } from './root/root.reducer';
import { AuthState } from './auth/auth.reducer';

export type StateSelector<T, S> = OutputSelector<AppState, T, (res: S) => T>;

export const appSelectors = {
    fromPromotionsState: <T>(
        selector: Selector<PromotionsState, T>
    ): StateSelector<T, PromotionsState> => {
        return createSelector(
            ({ promotions }: AppState): PromotionsState => promotions,
            selector
        );
    },

    fromPromotionState: <T>(
        selector: Selector<PromotionState, T>
    ): StateSelector<T, PromotionState> => {
        return createSelector(
            ({ promotion }: AppState): PromotionState => promotion,
            selector
        );
    },

    fromShopState: <T>(selector: Selector<ShopState, T>): StateSelector<T, ShopState> => {
        return createSelector(
            ({ shop }: AppState): ShopState => shop,
            selector
        );
    },

    fromContactsState: <T>(
        selector: Selector<ContactsState, T>
    ): StateSelector<T, ContactsState> => {
        return createSelector(
            ({ contacts }: AppState): ContactsState => contacts,
            selector
        );
    },

    fromAboutState: <T>(selector: Selector<AboutState, T>): StateSelector<T, AboutState> => {
        return createSelector(
            ({ about }: AppState): AboutState => about,
            selector
        );
    },

    fromShopsState: <T>(selector: Selector<ShopsState, T>): StateSelector<T, ShopsState> => {
        return createSelector(
            ({ shops }: AppState): ShopsState => shops,
            selector
        );
    },

    fromRootState: <T>(selector: Selector<RootState, T>): StateSelector<T, RootState> => {
        return createSelector(
            ({ root }: AppState): RootState => root,
            selector
        );
    },

    fromAuthState: <T>(selector: Selector<AuthState, T>): StateSelector<T, AuthState> => {
        return createSelector(
            ({ auth }: AppState): AuthState => auth,
            selector
        );
    }
};
