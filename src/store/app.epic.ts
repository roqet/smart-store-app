import { combineEpics } from 'redux-observable';
import { promotionsEpics } from './promotions/promotions.epics';
import { promotionEpics } from './promotion/promotion.epics';
import { contactsEpics } from './contacts/contacts.epics';
import { aboutEpics } from './about/about.epics';
import { shopsEpics } from './shops/shops.epics';
import { shopEpics } from './shop/shop.epics';
import { rootEpics } from './root/root.epics';
import { authEpics } from './auth/auth.epics';

export const appEpic = combineEpics(
    promotionsEpics,
    promotionEpics,
    contactsEpics,
    aboutEpics,
    shopsEpics,
    shopEpics,
    rootEpics,
    authEpics
);
