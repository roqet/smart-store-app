import { Action } from 'redux';

export enum Types {
    LOADING = '[PROMOTIONS] LOADING',
    LOAD_PROMOTIONS = '[PROMOTIONS] LOAD_PROMOTIONS',
    PROMOTIONS_LOADED = '[PROMOTIONS] PROMOTIONS_LOADED',
    REFRESH_PROMOTIONS = '[REFRESH] REFRESH_PROMOTIONS',
    PROMOTIONS_REFRESHED = '[PROMOTIONS] PROMOTIONS_REFRESHED'
}

export class Loading implements Action {
    public readonly type = Types.LOADING;
    public constructor(public loading: boolean) {}
}

export class LoadPromotions implements Action {
    public readonly type = Types.LOAD_PROMOTIONS;
}

export class PromotionsLoaded implements Action {
    public readonly type = Types.PROMOTIONS_LOADED;
    public constructor(public data: Store.Api.PagedResponse<Store.Promotion>) {}
}

export class RefreshPromotions implements Action {
    public readonly type = Types.REFRESH_PROMOTIONS;
}

export class PromotionsRefreshed implements Action {
    public readonly type = Types.PROMOTIONS_REFRESHED;
    public constructor(public data: Store.Api.PagedResponse<Store.Promotion>) {}
}

export type All = Types &
    Loading &
    LoadPromotions &
    PromotionsLoaded &
    RefreshPromotions &
    PromotionsRefreshed;
