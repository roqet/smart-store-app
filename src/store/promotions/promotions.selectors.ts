import { PromotionsState } from './promotions.reducer';
import { appSelectors } from '../app.selectors';

const select = appSelectors.fromPromotionsState;

const Selectors = {
    loading: (state: PromotionsState): boolean => state.loading,
    refreshing: (state: PromotionsState): boolean => state.refreshing,
    params: (state: PromotionsState): Store.Api.PaginationParams => state.params,
    data: (state: PromotionsState): Store.Api.PagedResponse<Store.Promotion> => state.data
};

export const PromotionsSelectors = {
    loading: select(Selectors.loading),
    refreshing: select(Selectors.refreshing),
    params: select(Selectors.params),
    data: select(Selectors.data)
};
