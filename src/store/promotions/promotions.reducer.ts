import merge from 'lodash/merge';
import { Types, All } from './promotions.actions';

export interface PromotionsState {
    loading: boolean;
    refreshing: boolean;
    params: Store.Api.PaginationParams;
    data: Store.Api.PagedResponse<Store.Promotion>;
}

export const promotionsInitialState: PromotionsState = {
    loading: false,
    refreshing: false,
    params: {
        count: 25,
        page: -1
    },
    data: {
        total: 0,
        records: []
    }
};

export function promotionsReducer(
    state: PromotionsState = promotionsInitialState,
    action: All
): PromotionsState {
    switch (action.type) {
        case Types.LOADING:
            return { ...state, loading: action.loading };
        case Types.LOAD_PROMOTIONS:
            return merge({}, state, {
                loading: true,
                params: {
                    page: state.params.page + 1
                }
            });
        case Types.REFRESH_PROMOTIONS:
            return merge({}, state, {
                refreshing: true,
                params: {
                    page: 0
                }
            });
        case Types.PROMOTIONS_LOADED:
            return merge({}, state, {
                loading: false,
                data: {
                    total: action.data.total,
                    records: [...state.data.records, ...action.data.records]
                }
            });
        case Types.PROMOTIONS_REFRESHED:
            return merge({}, state, {
                refreshing: false,
                data: {
                    total: action.data.total,
                    records: action.data.records
                }
            });
        default:
            return state;
    }
}
