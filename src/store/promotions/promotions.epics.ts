import { Observable, of } from 'rxjs';
import { Action } from 'redux';
import { ofType, combineEpics, StateObservable, ActionsObservable } from 'redux-observable';
import { map, flatMap, catchError } from 'rxjs/operators';
import { SmartApi } from '@common/api';
import { PromotionsSelectors } from './promotions.selectors';
import * as PromotionsActions from './promotions.actions';
import * as RootActions from '../root/root.actions';

const loadPromotions = (params: Store.Api.PaginationParams): Observable<Action> => {
    return SmartApi.promotions.query(params).pipe(
        map(
            (promotions): PromotionsActions.PromotionsLoaded => {
                return new PromotionsActions.PromotionsLoaded(promotions);
            }
        ),
        catchError(
            ({ errors }): Observable<Action> => {
                return of(new RootActions.HttpError(errors), new PromotionsActions.Loading(false));
            }
        )
    );
};

const loadPromotionsEpic = (
    action$: ActionsObservable<Action>,
    state$: StateObservable<AppState>
): Observable<Action> => {
    return action$.pipe(
        ofType(PromotionsActions.Types.LOAD_PROMOTIONS),
        map(
            (): Store.Api.PaginationParams => {
                return PromotionsSelectors.params(state$.value);
            }
        ),
        flatMap((params): Observable<Action> => loadPromotions(params))
    );
};

const refreshPromotionsEpic = (
    action$: ActionsObservable<Action>,
    state$: StateObservable<AppState>
): Observable<Action> => {
    return action$.pipe(
        ofType(PromotionsActions.Types.REFRESH_PROMOTIONS),
        map(
            (): Store.Api.PaginationParams => {
                return PromotionsSelectors.params(state$.value);
            }
        ),
        flatMap(
            (params): Observable<Store.Api.PagedResponse<Store.Promotion>> =>
                SmartApi.promotions.query(params)
        ),
        map(
            (promotions): PromotionsActions.PromotionsRefreshed => {
                return new PromotionsActions.PromotionsRefreshed(promotions);
            }
        )
    );
};

export const promotionsEpics = combineEpics(loadPromotionsEpic, refreshPromotionsEpic);
