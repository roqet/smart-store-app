import { Action } from 'redux';

export enum Types {
    LOADING = '[CONTACTS] LOADING',
    LOAD_CONTACTS = '[CONTACTS] LOAD_CONTACTS',
    CONTACTS_LOADED = '[CONTACTS] CONTACTS_LOADED'
}

export class Loading implements Action {
    public readonly type = Types.LOADING;
    public constructor(public loading: boolean) {}
}

export class LoadContacts implements Action {
    public readonly type = Types.LOAD_CONTACTS;
}

export class ContactsLoaded implements Action {
    public readonly type = Types.CONTACTS_LOADED;
    public constructor(public contacts: Store.Contacts) {}
}

export type All = Types & Loading & LoadContacts & ContactsLoaded;
