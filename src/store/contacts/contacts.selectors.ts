import { ContactsState } from './contacts.reducer';
import { appSelectors } from '../app.selectors';

const select = appSelectors.fromContactsState;

const Selectors = {
    loading: (state: ContactsState): boolean => state.loading,
    initialized: (state: ContactsState): boolean => state.initialized,
    contacts: (state: ContactsState): Store.Contacts => state.contacts
};

export const ContactsSelectors = {
    loading: select(Selectors.loading),
    initialized: select(Selectors.initialized),
    contacts: select(Selectors.contacts)
};
