import { Types, All } from './contacts.actions';

export interface ContactsState {
    initialized: boolean;
    loading: boolean;
    contacts: Store.Contacts;
}

export const contactsInitialState: ContactsState = {
    initialized: false,
    loading: false,
    contacts: null
};

export function contactsReducer(
    state: ContactsState = contactsInitialState,
    action: All
): ContactsState {
    switch (action.type) {
        case Types.LOADING:
            return { ...state, loading: action.loading };
        case Types.LOAD_CONTACTS:
            return { ...state, loading: true };
        case Types.CONTACTS_LOADED:
            return { ...state, initialized: true, contacts: action.contacts };
        default:
            return state;
    }
}
