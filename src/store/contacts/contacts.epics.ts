import { Observable, of } from 'rxjs';
import { Action } from 'redux';
import { ofType, combineEpics, ActionsObservable } from 'redux-observable';
import { map, flatMap, catchError } from 'rxjs/operators';
import { SmartApi, Pages } from '@common/api';
import * as ContactsActions from './contacts.actions';
import * as RootActions from '../root/root.actions';

const loadContacts = (): Observable<Action> => {
    return SmartApi.page.get<Store.Contacts>(Pages.contacts).pipe(
        map(
            (contacts): ContactsActions.ContactsLoaded => {
                return new ContactsActions.ContactsLoaded(contacts);
            }
        ),
        catchError(
            ({ errors }): Observable<Action> => {
                return of(new RootActions.HttpError(errors), new ContactsActions.Loading(false));
            }
        )
    );
};

const loadContactsEpic = (action$: ActionsObservable<Action>): Observable<Action> => {
    return action$.pipe(
        ofType(ContactsActions.Types.LOAD_CONTACTS),
        flatMap((): Observable<Action> => loadContacts())
    );
};

export const contactsEpics = combineEpics(loadContactsEpic);
