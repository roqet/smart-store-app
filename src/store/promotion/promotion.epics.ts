import { Observable, of } from 'rxjs';
import { Action } from 'redux';
import { ofType, combineEpics, ActionsObservable } from 'redux-observable';
import { map, flatMap, catchError } from 'rxjs/operators';
import { SmartApi } from '@common/api';
import * as PromotionActions from './promotion.actions';
import * as RootActions from '../root/root.actions';

const loadPromotion = (id: number): Observable<Action> => {
    return SmartApi.promotions.get(id).pipe(
        map(
            (promotion): PromotionActions.PromotionLoaded => {
                return new PromotionActions.PromotionLoaded(promotion);
            }
        ),
        catchError(
            ({ errors }): Observable<Action> => {
                return of(new RootActions.HttpError(errors), new PromotionActions.Loading(false));
            }
        )
    );
};

const loadPromotionEpic = (
    action$: ActionsObservable<PromotionActions.LoadPromotion>
): Observable<Action> => {
    return action$.pipe(
        ofType(PromotionActions.Types.LOAD_PROMOTION),
        flatMap((action): Observable<Action> => loadPromotion(action.id))
    );
};

export const promotionEpics = combineEpics(loadPromotionEpic);
