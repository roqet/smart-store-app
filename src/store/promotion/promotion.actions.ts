import { Action } from 'redux';

export enum Types {
    LOADING = '[PROMOTION] LOADING',
    LOAD_PROMOTION = '[PROMOTION] LOAD_PROMOTION',
    PROMOTION_LOADED = '[PROMOTION] PROMOTION_LOADED',
    INITIALIZE = '[PROMOTION] INITIALIZE'
}

export class Loading implements Action {
    public readonly type = Types.LOADING;
    public constructor(public loading: boolean) {}
}

export class Initialize implements Action {
    public readonly type = Types.INITIALIZE;
    public constructor(public initialized: boolean) {}
}

export class LoadPromotion implements Action {
    public readonly type = Types.LOAD_PROMOTION;
    public constructor(public id: number) {}
}

export class PromotionLoaded implements Action {
    public readonly type = Types.PROMOTION_LOADED;
    public constructor(public promotion: Store.Promotion) {}
}

export type All = Types & Loading & LoadPromotion & PromotionLoaded & Initialize;
