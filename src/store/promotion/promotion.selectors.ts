import { PromotionState } from './promotion.reducer';
import { appSelectors } from '../app.selectors';

const Selectors = {
    loading: (state: PromotionState): boolean => state.loading,
    initialized: (state: PromotionState): boolean => state.initialized,
    promotion: (state: PromotionState): Store.Promotion => state.promotion
};

export const PromotionSelectors = {
    loading: appSelectors.fromPromotionState(Selectors.loading),
    initialized: appSelectors.fromPromotionState(Selectors.initialized),
    promotion: appSelectors.fromPromotionState(Selectors.promotion)
};
