import { Types, All } from './promotion.actions';

export interface PromotionState {
    loading: boolean;
    initialized: boolean;
    promotion: Store.Promotion;
}

export const promotionInitialState: PromotionState = {
    loading: false,
    initialized: false,
    promotion: null
};

export function promotionReducer(
    state: PromotionState = promotionInitialState,
    action: All
): PromotionState {
    switch (action.type) {
        case Types.LOADING:
            return { ...state, loading: action.loading };
        case Types.LOAD_PROMOTION:
            return { ...state, loading: true };
        case Types.INITIALIZE:
            return { ...state, initialized: action.initialized };
        case Types.PROMOTION_LOADED:
            return { loading: false, initialized: true, promotion: action.promotion };
        default:
            return state;
    }
}
