import { Types, All } from './shop.actions';

export interface ShopState {
    loading: boolean;
    shop: Store.Shop;
}

export const shopInitialState: ShopState = {
    loading: false,
    shop: null
};

export function shopReducer(state: ShopState = shopInitialState, action: All): ShopState {
    switch (action.type) {
        case Types.LOADING:
            return { ...state, loading: false };
        case Types.LOAD_SHOP:
            return { ...state, loading: true };
        case Types.SHOP_LOADED:
            return { loading: false, shop: action.shop };
        default:
            return state;
    }
}
