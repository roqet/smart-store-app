import { Observable, of } from 'rxjs';
import { Action } from 'redux';
import { ofType, combineEpics, ActionsObservable } from 'redux-observable';
import { map, flatMap, catchError } from 'rxjs/operators';
import { SmartApi } from '@common/api';
import * as ShopActions from './shop.actions';
import * as RootActions from '../root/root.actions';

const loadShop = (id: number): Observable<Action> => {
    return SmartApi.shop.get(id).pipe(
        map(
            (shop): ShopActions.ShopLoaded => {
                return new ShopActions.ShopLoaded(shop);
            }
        ),
        catchError(
            ({ errors }): Observable<Action> => {
                return of(new RootActions.HttpError(errors), new ShopActions.Loading(false));
            }
        )
    );
};

const loadShopEpic = (action$: ActionsObservable<ShopActions.LoadShop>): Observable<Action> => {
    return action$.pipe(
        ofType(ShopActions.Types.LOAD_SHOP),
        flatMap((action): Observable<Action> => loadShop(action.id))
    );
};

export const shopEpics = combineEpics(loadShopEpic);
