import { Action } from 'redux';

export enum Types {
    LOADING = '[SHOP] LOADING',
    LOAD_SHOP = '[SHOP] LOAD_PROMOTION',
    SHOP_LOADED = '[SHOP] PROMOTION_LOADED',
    INITIALIZE = '[SHOP] INITIALIZE'
}

export class Loading implements Action {
    public readonly type = Types.LOADING;
    public constructor(public loading: boolean) {}
}

export class Initialize implements Action {
    public readonly type = Types.INITIALIZE;
    public constructor(public initialized: boolean) {}
}

export class LoadShop implements Action {
    public readonly type = Types.LOAD_SHOP;
    public constructor(public id: number) {}
}

export class ShopLoaded implements Action {
    public readonly type = Types.SHOP_LOADED;
    public constructor(public shop: Store.Shop) {}
}

export type All = Types & Loading & LoadShop & ShopLoaded & Initialize;
