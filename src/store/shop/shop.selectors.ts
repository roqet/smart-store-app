import { ShopState } from './shop.reducer';
import { appSelectors } from '../app.selectors';

const Selectors = {
    loading: (state: ShopState): boolean => state.loading,
    shop: (state: ShopState): Store.Shop => state.shop
};

export const ShopSelectors = {
    loading: appSelectors.fromShopState(Selectors.loading),
    shop: appSelectors.fromShopState(Selectors.shop)
};
