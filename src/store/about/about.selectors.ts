import { AboutState } from './about.reducer';
import { appSelectors } from '../app.selectors';

const select = appSelectors.fromAboutState;

const Selectors = {
    loading: (state: AboutState): boolean => state.loading,
    initialized: (state: AboutState): boolean => state.initialized,
    about: (state: AboutState): Store.About => state.about
};

export const AboutSelectors = {
    loading: select(Selectors.loading),
    initialized: select(Selectors.initialized),
    about: select(Selectors.about)
};
