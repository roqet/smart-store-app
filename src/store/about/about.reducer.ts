import { Types, All } from './about.actions';

export interface AboutState {
    loading: boolean;
    initialized: boolean;
    about: Store.About;
}

export const aboutInitialState: AboutState = {
    loading: false,
    initialized: false,
    about: {
        type: '',
        content: ''
    }
};

export function aboutReducer(state: AboutState = aboutInitialState, action: All): AboutState {
    switch (action.type) {
        case Types.LOADING:
            return { ...state, loading: action.loading };
        case Types.LOAD_ABOUT:
            return { ...state, loading: true };
        case Types.ABOUT_LOADED:
            return { ...state, initialized: true, about: action.about };
        default:
            return state;
    }
}
