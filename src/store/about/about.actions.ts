import { Action } from 'redux';

export enum Types {
    LOADING = '[ABOUT] LOADING',
    LOAD_ABOUT = '[ABOUT] LOAD_ABOUT',
    ABOUT_LOADED = '[ABOUT] CONTACTS_LOADED'
}

export class Loading implements Action {
    public readonly type = Types.LOADING;
    public constructor(public loading: boolean) {}
}

export class LoadAbout implements Action {
    public readonly type = Types.LOAD_ABOUT;
}

export class AboutLoaded implements Action {
    public readonly type = Types.ABOUT_LOADED;
    public constructor(public about: Store.About) {}
}

export type All = Types & Loading & LoadAbout & AboutLoaded;
