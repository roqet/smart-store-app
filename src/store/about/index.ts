import * as AboutActions from './about.actions';

export * from './about.reducer';
export * from './about.selectors';
export { AboutActions };
