import { Observable, of } from 'rxjs';
import { Action } from 'redux';
import { ofType, combineEpics, ActionsObservable } from 'redux-observable';
import { map, catchError, mergeMap } from 'rxjs/operators';
import { SmartApi, Pages } from '@common/api';
import * as AboutActions from './about.actions';
import * as RootActions from '../root/root.actions';

const loadAbout = (): Observable<Action> => {
    return SmartApi.page.get<Store.About>(Pages.about).pipe(
        map(
            (about): AboutActions.AboutLoaded => {
                return new AboutActions.AboutLoaded(about);
            }
        ),
        catchError(
            ({ errors }): Observable<Action> => {
                return of(new AboutActions.Loading(false), new RootActions.HttpError(errors));
            }
        )
    );
};

const loadAboutEpic = (action$: ActionsObservable<Action>): Observable<Action> => {
    return action$.pipe(
        ofType(AboutActions.Types.LOAD_ABOUT),
        mergeMap((): Observable<Action> => loadAbout())
    );
};

export const aboutEpics = combineEpics(loadAboutEpic);
