/* eslint-disable import/no-cycle */
import merge from 'lodash/merge';
import { Types, All } from './auth.actions';

export type AuthSteps = 'auth.tel' | 'auth.code' | 'auth.card' | 'auth.regStep1' | 'auth.regStep2';

export type AuthData = Store.User;

export interface AuthState {
    step: AuthSteps;
    loading: boolean;
    data: AuthData;
}

export const aboutInitialState: AuthState = {
    step: 'auth.tel',
    loading: false,
    data: null
};

export function authReducer(state: AuthState = aboutInitialState, action: All): AuthState {
    switch (action.type) {
        case Types.LOADING:
            return { ...state, loading: action.loading };
        case Types.NEXT:
            return { ...state, step: action.step };
        case Types.SEND_PASSWODR:
            return { ...state, loading: true };
        case Types.LOGIN:
            return { ...state, loading: true };
        case Types.PASSWODR_SENT:
            return merge({}, state, {
                step: 'auth.code',
                loading: false,
                data: { phoneNumber: action.tel }
            });
        case Types.UPDATE_DATA:
            return merge(
                {},
                state,
                {
                    data: action.data
                },
                action.step !== undefined ? { step: action.step } : {}
            );
        case Types.SET_STEP:
            return { ...state, step: action.step };
        case Types.CREATE_USER:
            return merge({}, state, {
                loading: true,
                data: action.data
            });
        default:
            return state;
    }
}
