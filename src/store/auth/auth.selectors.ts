import { AuthState, AuthSteps, AuthData } from './auth.reducer';
import { appSelectors } from '../app.selectors';

const Selectors = {
    loading: (state: AuthState): boolean => state.loading,
    step: (state: AuthState): AuthSteps => state.step,
    data: (state: AuthState): AuthData => state.data
};

export const AuthSelectors = {
    loading: appSelectors.fromAuthState(Selectors.loading),
    step: appSelectors.fromAuthState(Selectors.step),
    data: appSelectors.fromAuthState(Selectors.data)
};
