/* eslint-disable import/no-cycle */
import { Observable, of } from 'rxjs';
import { Action } from 'redux';
import { ofType, combineEpics, ActionsObservable, StateObservable } from 'redux-observable';
import { map, flatMap, catchError, mergeMap } from 'rxjs/operators';
import { SmartApi } from '@common/api';
import { Auth } from '@common/services';
import { RootActions } from '../root';
import { AuthData } from './auth.reducer';
import { AuthSelectors } from './auth.selectors';
import * as AuthActions from './auth.actions';

interface LoginData {
    phoneNumber: string;
    password: string;
}

const sendPassword = (phoneNumber: string): Observable<Action> => {
    return SmartApi.auth.sendPassword(phoneNumber).pipe(
        mergeMap((): Action[] => {
            return [
                new AuthActions.UpdateData({ phoneNumber }),
                new AuthActions.Loading(false),
                new AuthActions.Next('auth.code')
            ];
        }),
        catchError(
            ({ errors }): Observable<Action> => {
                return of(new RootActions.HttpError(errors), new AuthActions.Loading(false));
            }
        )
    );
};

const sendPasswordEpic = (
    action$: ActionsObservable<AuthActions.SendPassword>
): Observable<Action> => {
    return action$.pipe(
        ofType(AuthActions.Types.SEND_PASSWODR),
        flatMap((action): Observable<Action> => sendPassword(action.tel))
    );
};

const getCurrentUser = (): Observable<Store.User> => {
    return SmartApi.user.current().pipe(
        map(
            (user): Store.User => {
                return user.firstName ? user : null;
            }
        )
    );
};

const loginEpic = (
    action$: ActionsObservable<AuthActions.Login>,
    state$: StateObservable<AppState>
): Observable<Action> => {
    return action$.pipe(
        ofType(AuthActions.Types.LOGIN),
        map(
            (action): LoginData => ({
                password: action.code,
                phoneNumber: AuthSelectors.data(state$.value).phoneNumber
            })
        ),
        flatMap((data): Observable<string> => SmartApi.auth.login(data)),
        flatMap((token): Observable<string> => Auth.saveToken(token)),
        flatMap((): Observable<Store.User> => getCurrentUser()),
        mergeMap((user): Action[] => {
            if (!user) {
                return [new AuthActions.Loading(false), new AuthActions.Next('auth.regStep1')];
            }
            return [new AuthActions.Loading(false), new RootActions.Initialized(user)];
        })
    );
};

const createUser = (data): Observable<Action> => {
    return SmartApi.user.create(data).pipe(
        mergeMap((user): Action[] => {
            return [new AuthActions.Loading(false), new RootActions.Initialized(user)];
        }),
        catchError(
            ({ errors }): Observable<Action> => {
                return of(new RootActions.HttpError(errors), new AuthActions.Loading(false));
            }
        )
    );
};

const createUserEpic = (
    action$: ActionsObservable<AuthActions.CreateUser>,
    state$: StateObservable<AppState>
): Observable<Action> => {
    return action$.pipe(
        ofType(AuthActions.Types.CREATE_USER),
        map((): AuthData => AuthSelectors.data(state$.value)),
        flatMap((data): Observable<Action> => createUser(data))
    );
};

export const authEpics = combineEpics(sendPasswordEpic, loginEpic, createUserEpic);
