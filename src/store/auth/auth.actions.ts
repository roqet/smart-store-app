/* eslint-disable import/no-cycle */
import { Action } from 'redux';
import { AuthSteps } from './auth.reducer';

export enum Types {
    NEXT = '[AUTH] NEXT',
    LOADING = '[AUTH] LOADING',
    SET_STEP = '[AUTH] SET_STEP',
    UPDATE_DATA = '[AUTH] UPDATE_DATA',
    SEND_PASSWODR = '[AUTH] SEND_PASSWODR',
    PASSWODR_SENT = '[AUTH] PASSWODR_SENT',
    LOGIN = '[AUTH] LOGIN',
    CREATE_USER = '[AUTH] CREATE_USER'
}

export class Next implements Action {
    public readonly type = Types.NEXT;
    public constructor(public step: AuthSteps) {}
}

export class Loading implements Action {
    public readonly type = Types.LOADING;
    public constructor(public loading: boolean) {}
}

export class SetStep implements Action {
    public readonly type = Types.SET_STEP;
    public constructor(public step: AuthSteps) {}
}

export class UpdateData implements Action {
    public readonly type = Types.UPDATE_DATA;
    public constructor(public data: Partial<Store.User>, public step?: AuthSteps) {}
}

export class SendPassword implements Action {
    public readonly type = Types.SEND_PASSWODR;
    public constructor(public tel: string) {}
}

export class PasswordSent implements Action {
    public readonly type = Types.PASSWODR_SENT;
    public constructor(public tel: string) {}
}

export class Login implements Action {
    public readonly type = Types.LOGIN;
    public constructor(public code: string) {}
}

export class CreateUser implements Action {
    public readonly type = Types.CREATE_USER;
    public constructor(public data: Partial<Store.User>) {}
}

export type All = Types & Next & Loading & SendPassword & PasswordSent & UpdateData & SetStep;
