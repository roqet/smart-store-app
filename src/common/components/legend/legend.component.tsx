import React from 'react';
import { View, Text } from 'native-base';
import { i18n } from '../../services';
import { styles } from './legend.styles';

interface LegendItem {
    style: object;
    label: string;
    align: object;
}

export function Legend(): JSX.Element {
    const legend: LegendItem[] = [
        {
            style: styles.delivery,
            label: i18n.t('forms.dayOfWeightProductUpdate'),
            align: { justifyContent: 'flex-start' }
        },
        {
            style: styles.lux,
            label: i18n.t('forms.luxuryGoodsDay'),
            align: { justifyContent: 'flex-end' }
        }
    ];

    return (
        <View style={styles.container}>
            {legend.map(
                ({ style, label, align }): JSX.Element => {
                    return (
                        <View key={label} style={{ ...styles.item, ...align }}>
                            <View style={{ ...style, ...styles.color }} />
                            <Text style={styles.text}>{label}</Text>
                        </View>
                    );
                }
            )}
        </View>
    );
}
