import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    delivery: {
        backgroundColor: '$black'
    },
    lux: {
        backgroundColor: '$primary'
    },
    container: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    item: {
        flex: 1,
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    color: {
        width: 16,
        height: 16,
        marginRight: 15
    },
    text: {
        fontSize: 12,
        lineHeight: 14,
        color: '$dustyGray',
        fontFamily: 'Roboto'
    }
});
