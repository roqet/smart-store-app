import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import { styles } from './loader.styles';

interface LoaderProps {
    show: boolean;
    size?: 'small' | 'large';
}

export function Loader({ show, size = 'small' }: LoaderProps): JSX.Element | null {
    return show ? (
        <View style={styles.loader}>
            <ActivityIndicator size={size} />
        </View>
    ) : null;
}
