import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    separator: {
        height: 1,
        backgroundColor: '#E5E5E5'
    }
});
