import React from 'react';
import { View } from 'react-native';
import { styles } from './separator.styles';

export function Separator(): JSX.Element {
    return <View style={styles.separator} />;
}
