import React from 'react';
import { StyleSheetProperties, Platform } from 'react-native';
import { connectStyle } from 'native-base-shoutem-theme';
import mapPropsToStyleNames from 'native-base/src/utils/mapPropsToStyleNames';
import SmartIcon from './icon.template';

import { Icons } from '../../constants';

const ic = {};

interface IconProps {
    type?: string;
    name: Icons;
    active?: boolean;
    ios?: string;
    android?: string;
    style?: StyleSheetProperties;
    size?: number;
}

function Icon({ type, name, active, ios, android, style, size }: IconProps): JSX.Element {
    const props = { type, name, active, ios, android, style, size };

    const getIconName = (): string => {
        if (Platform.OS === 'ios') {
            if (ios) {
                return ios;
            }
            return active ? ic[name].ios.active : ic[name].ios.default;
        }
        if (android) {
            return android;
        }
        return active ? ic[name].android.active : ic[name].android.default;
    };

    const both = ((show): JSX.Element | null => {
        return show ? <SmartIcon {...props} name={Platform.OS === 'ios' ? ios : android} /> : null;
    })(ios && android);

    const some = ((show): JSX.Element | null => {
        return show ? <SmartIcon {...props} name={getIconName()} /> : null;
    })(name && (android || ios));

    const def = <SmartIcon {...props} />;

    return (
        <>
            {both}
            {some}
            {def}
        </>
    );
}

const StyledIcon = connectStyle('NativeBase.Icon', {}, mapPropsToStyleNames)(Icon);

export { StyledIcon as Icon };
