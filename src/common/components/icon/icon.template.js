/**
 * SmartStore icon set component.
 * Usage: <SmartStore name="icon-name" size={20} color="#4F8EF7" />
 */

import createIconSet from 'react-native-vector-icons/lib/create-icon-set';
const glyphMap = {
  "barcode": 61697,
  "bell": 61698,
  "call-answer": 61699,
  "checkmark": 61700,
  "chevron-left": 61701,
  "close": 61702,
  "contact": 61703,
  "edit": 61704,
  "facebook": 61705,
  "gift": 61706,
  "home": 61707,
  "instagram": 61708,
  "location": 61709,
  "marker": 61710,
  "percent": 61711,
  "polygon": 61712,
  "qwe": 61713,
  "team": 61714,
  "telegram": 61715,
  "user": 61716,
  "x": 61717
};

const iconSet = createIconSet(glyphMap, 'SmartStore', 'SmartStore.ttf');

export default iconSet;

export const Button = iconSet.Button;
export const TabBarItem = iconSet.TabBarItem;
export const TabBarItemIOS = iconSet.TabBarItemIOS;
export const ToolbarAndroid = iconSet.ToolbarAndroid;
export const getImageSource = iconSet.getImageSource;
