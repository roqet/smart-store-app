import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    vertical: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    radioOuter: {
        borderWidth: 2,
        borderColor: '$black',
        padding: 2
    },
    radioOuterM: {
        width: 28,
        height: 28,
        borderRadius: 14
    },
    radioInner: {
        backgroundColor: '$black'
    },
    radioInnerM: {
        width: 20,
        height: 20,
        borderRadius: 10
    },
    label: {
        color: 'rgba(0, 0, 0, 0.7)',
        paddingTop: 9,
        paddingBottom: 9
    },
    labelM: {
        fontSize: 18
    }
});
