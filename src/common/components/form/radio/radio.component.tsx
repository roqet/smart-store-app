import React, { useState } from 'react';
import { TouchableOpacity, ViewStyle } from 'react-native';
import { View, Text } from 'native-base';
import { iorientation, isize } from '../form.types';
import { styles } from './radio.styles';

interface RadioProps {
    orientation?: iorientation;
    label: string;
    size?: isize;
    style?: ViewStyle;
    value: string | number;
    selected: string | number;
    onChange?: (value: string | number) => void;
}

const radioOuterStyles = (size: isize): ViewStyle => ({
    ...styles.radioOuter,
    ...styles[`radioOuter${size}`]
});

const radioInnerStyles = (size: isize): ViewStyle => ({
    ...styles.radioInner,
    ...styles[`radioInner${size}`]
});

const labelStyles = (size: isize): ViewStyle => ({
    ...styles.label,
    ...styles[`label${size}`]
});

export function Radio({
    value,
    label,
    selected,
    size = 'M',
    style = {},
    orientation = 'vertical',
    onChange = (): void => {}
}: RadioProps): JSX.Element {
    const inner = value === selected ? <View style={radioInnerStyles(size)} /> : null;

    return (
        <TouchableOpacity
            style={{ ...styles[orientation], ...style }}
            onPress={(): void => onChange(selected)}
        >
            <View style={radioOuterStyles(size)}>{inner}</View>
            <Text style={labelStyles(size)}>{label.toUpperCase()}</Text>
        </TouchableOpacity>
    );
}
