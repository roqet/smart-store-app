import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    group: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingTop: 10
    }
});
