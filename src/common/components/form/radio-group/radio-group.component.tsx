import React, { PropsWithChildren } from 'react';
import { View } from 'native-base';
import { styles } from './radio-group.styles';

export function RadioGroup({ children }: PropsWithChildren<{}>): JSX.Element {
    return <View style={styles.group}>{children}</View>;
}
