import { TextStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { isize } from '../../form.types';

const styles = StyleSheet.create({
    note: {
        color: '$black',
        marginTop: -30
    },
    noteL: {
        fontSize: 14,
        lineHeight: 22
    }
});

export const noteStyles = (size: isize): TextStyle => ({
    ...styles.note,
    ...styles[`note${size}`]
});
