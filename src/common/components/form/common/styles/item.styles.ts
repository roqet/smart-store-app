import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { isize } from '../../form.types';

const styles = StyleSheet.create({
    item: {
        borderBottomColor: '$white',
        borderBottomWidth: 2,
        position: 'relative'
    },
    itemDisabled: {
        borderBottomColor: 'transparent'
    },
    itemL: {
        marginBottom: 50
    },
    itemM: {
        marginBottom: 20
    }
});

export const itemStyles = (size: isize, disabled: boolean): ViewStyle => ({
    ...styles.item,
    ...styles[`item${size}`],
    ...(disabled ? styles.itemDisabled : {})
});
