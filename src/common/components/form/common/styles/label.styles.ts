import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { isize } from '../../form.types';

const styles = StyleSheet.create({
    label: {
        fontSize: 16
    },
    labelM: {
        fontSize: 18
    },
    labelFocusedM: {
        color: '#000000',
        fontSize: 14
    },
    labelL: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    labelFocusedL: {
        color: '#000000',
        opacity: 1
    }
});

export const labelStyles = (size: isize, focused: boolean): ViewStyle => ({
    ...styles.label,
    ...styles[`label${size}`],
    ...(focused ? styles[`labelFocused${size}`] : {})
});
