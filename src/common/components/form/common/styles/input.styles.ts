import { ViewStyle, Platform } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { isize } from '../../form.types';

const styles = StyleSheet.create({
    input: {},
    inputM: {},
    inputL: {
        fontSize: 28,
        lineHeight: 34,
        height: 68,
        ...(Platform.OS === 'ios' ? { paddingBottom: 23, paddingTop: 23 } : {})
    }
});

export const inputStyles = (size: isize): ViewStyle => ({
    ...styles.input,
    ...styles[`input${size}`]
});
