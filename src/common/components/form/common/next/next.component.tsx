import React, { PropsWithChildren } from 'react';
import { View } from 'native-base';
import { isize } from '../../form.types';
import { nextStyles } from './next.styles';

interface NextProps {
    size: isize;
}

export function Next({ children, size }: PropsWithChildren<NextProps>): JSX.Element {
    return <View style={nextStyles(size)}>{children}</View>;
}
