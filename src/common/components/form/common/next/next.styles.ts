import { ViewStyle } from 'react-native';
import StyleSheet from 'react-native-extended-stylesheet';
import { isize } from '../../form.types';

const styles = StyleSheet.create({
    next: {
        position: 'absolute',
        right: 0
    },
    nextL: {
        top: 30
    }
});

export const nextStyles = (size: isize): ViewStyle => ({
    ...styles.next,
    ...styles[`next${size}`]
});
