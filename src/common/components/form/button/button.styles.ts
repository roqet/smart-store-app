import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    button: {
        height: 68,
        borderRadius: 5,
        marginBottom: 40,
        paddingLeft: 10,
        paddingRight: 10
    },
    disabled: {
        opacity: 0.7
    },
    buttonlight: {
        backgroundColor: '$white'
    },
    buttondark: {
        backgroundColor: '$black'
    },
    text: {
        fontSize: 18,
        textAlign: 'center'
    },
    textlight: {
        color: '$black'
    },
    textdark: {
        color: '$white'
    }
});
