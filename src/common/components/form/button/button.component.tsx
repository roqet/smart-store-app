import React from 'react';
import { ViewStyle } from 'react-native';
import { Button as NBButton, Text } from 'native-base';
import { styles } from './button.styles';

type itheme = 'light' | 'dark';

interface ButtonProps {
    label: string;
    onPress?: () => void;
    theme?: itheme;
    disabled?: boolean;
}

const buttonStyles = (theme: itheme, disabled: boolean): ViewStyle => ({
    ...styles.button,
    ...styles[`button${theme}`],
    ...(disabled ? styles.disabled : {})
});

const textStyles = (theme: itheme): ViewStyle => ({
    ...styles.text,
    ...styles[`text${theme}`]
});

export function Button({
    label,
    disabled,
    theme = 'light',
    onPress = (): void => {}
}: ButtonProps): JSX.Element {
    return (
        <NBButton
            block
            large
            disabled={disabled}
            style={buttonStyles(theme, disabled)}
            onPress={onPress}
        >
            <Text style={textStyles(theme)}>{label.toUpperCase()}</Text>
        </NBButton>
    );
}
