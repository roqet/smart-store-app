import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    heading: {
        fontSize: 22,
        fontWeight: 'bold',
        textAlign: 'center'
    }
});
