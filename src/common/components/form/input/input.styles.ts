import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    wrapper: {
        position: 'relative'
    },

    item: {
        borderBottomColor: '$white',
        borderBottomWidth: 2,
        position: 'relative'
    },
    itemDisabled: {
        borderBottomColor: 'transparent'
    },
    itemL: {
        marginBottom: 50
    },
    itemM: {
        marginBottom: 20
    },

    label: {
        fontSize: 16
    },
    labelM: {
        fontSize: 18
    },
    labelFocusedM: {
        color: '#000000',
        fontSize: 14
    },
    labelL: {
        fontSize: 16,
        fontWeight: 'bold'
    },
    labelFocusedL: {
        color: '#000000',
        opacity: 1
    },

    input: {},
    inputL: {
        fontSize: 28,
        lineHeight: 34,
        height: 68,
        paddingBottom: 23,
        paddingTop: 23
    },

    next: {
        position: 'absolute',
        right: 0
    },
    nextL: {
        top: 30
    }
});
