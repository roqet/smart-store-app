import React from 'react';
import { Button } from 'native-base';
import { Icons } from '../../../../constants';
import { Icon } from '../../../icon/icon.component';
import { styles } from './action.styles';

interface ActionProps {
    icon: Icons;
    onPress: () => void;
}

export function Action({ icon, onPress }: ActionProps): JSX.Element {
    return (
        <Button dark rounded small onPress={onPress} style={styles.button}>
            <Icon name={icon} style={styles.icon} />
        </Button>
    );
}
