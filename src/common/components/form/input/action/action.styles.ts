import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    button: {
        backgroundColor: 'rgba(0, 0, 0, 0.6)',
        minWidth: 24,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        color: '$white',
        fontSize: 10
    }
});
