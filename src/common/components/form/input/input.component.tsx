import React, { useState } from 'react';
import { KeyboardType } from 'react-native';
import { Item, Input as NBInput, Label, View, Text } from 'native-base';
import { Action } from './action/action.component';
import { labelStyles, itemStyles, inputStyles, noteStyles, Next } from '../common';
import { isize } from '../form.types';
import { styles } from './input.styles';

interface InputProps {
    type: KeyboardType;
    value?: string;
    label?: string;
    disabled?: boolean;
    next?: JSX.Element;
    size?: isize;
    note?: string;
    onChange?: (value) => void;
}

export function Input({
    label,
    note,
    type,
    next,
    disabled = false,
    value = '',
    size = 'M',
    onChange = (): void => {}
}: InputProps): JSX.Element {
    const [model, setModel] = useState(value);
    const [focused, setFocused] = useState(!!value);

    const labelEL = <Label style={labelStyles(size, focused)}>{label}</Label>;

    const noteEl = note && <Text style={noteStyles(size)}>{note}</Text>;

    const inputEL = (
        <NBInput
            style={inputStyles(size)}
            keyboardType={type}
            clearButtonMode={disabled ? 'never' : 'always'}
            value={model}
            disabled={disabled}
            onFocus={(): void => setFocused(true)}
            onBlur={(): void => setFocused(!!model)}
            onChangeText={(val): void => {
                setModel(val);
                onChange(val);
            }}
        />
    );

    const itemEL = (
        <Item floatingLabel style={itemStyles(size, disabled)}>
            {labelEL}
            {inputEL}
        </Item>
    );

    const nextEL = next && <Next size={size}>{next}</Next>;

    return (
        <View style={styles.wrapper}>
            {itemEL}
            {nextEL}
            {noteEl}
        </View>
    );
}

Input.Action = Action;
