export type isize = 'S' | 'M' | 'L';
export type iorientation = 'vertical' | 'horizontal';
