import React, { PropsWithChildren } from 'react';
import { Form as NBForm, View, Text } from 'native-base';
import { Input } from './input/input.component';
import { Datepicker } from './datepicker/datepicker.component';
import { RadioGroup } from './radio-group/radio-group.component';
import { Radio } from './radio/radio.component';
import { Checkbox } from './checkbox/checkbox.component';
import { Button } from './button/button.component';
import { styles } from './form.styles';

interface FormProps {
    title?: string;
    spacing?: number;
}

export function Form({ title, spacing = 22, children }: PropsWithChildren<FormProps>): JSX.Element {
    const heading = title && (
        <Text style={{ marginBottom: spacing, ...styles.heading }}>{title}</Text>
    );

    return (
        <View>
            {heading}
            <NBForm>{children}</NBForm>
        </View>
    );
}

Form.Input = Input;
Form.Datepicker = Datepicker;
Form.RadioGroup = RadioGroup;
Form.Radio = Radio;
Form.Checkbox = Checkbox;
Form.Button = Button;
