import React, { PropsWithChildren } from 'react';
import { Switch } from 'react-native';
import { View } from 'native-base';
import { styles } from './checkbox.styles';

interface CheckboxProps {
    value: boolean;
    onChange?: (value: boolean) => void;
}

export function Checkbox({
    children,
    value,
    onChange = (): void => {}
}: PropsWithChildren<CheckboxProps>): JSX.Element {
    return (
        <View style={styles.wrapper}>
            <View style={styles.label}>{children}</View>
            <Switch value={value} onValueChange={onChange} />
        </View>
    );
}
