import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    wrapper: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 20,
        marginBottom: 50
    },
    label: {
        flex: 1,
        paddingRight: 40
    },
    switch: {
        flex: 0
    }
});
