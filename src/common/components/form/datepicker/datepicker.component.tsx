import moment from 'moment';
import React, { useState } from 'react';
import { KeyboardType, ViewStyle } from 'react-native';
import { Item, Input as NBInput, Label, View, DatePicker } from 'native-base';
import { labelStyles, inputStyles, itemStyles, Next } from '../common';
import { isize } from '../form.types';
import { styles } from './datepicker.styles';

interface InputProps {
    type: KeyboardType;
    value?: number;
    label?: string;
    disabled?: boolean;
    next?: JSX.Element;
    size?: isize;
    onChange?: (value) => void;
}

const datepickerStyles = (size: isize): ViewStyle => ({
    ...styles.datepicker,
    ...styles[`datepicker${size}`]
});

export function Datepicker({
    label,
    type,
    next,
    disabled = false,
    value,
    size = 'M',
    onChange = (): void => {}
}: InputProps): JSX.Element {
    const [model, setModel] = useState(value ? moment(value).format('L') : '');
    const [focused, setFocused] = useState(!!value);

    const labelEL = <Label style={labelStyles(size, focused)}>{label}</Label>;

    const inputEL = (
        <NBInput
            editable={false}
            style={inputStyles(size)}
            keyboardType={type}
            clearButtonMode='never'
            value={model}
            disabled={disabled}
        />
    );

    const datepickerEl = !disabled && (
        <View style={datepickerStyles(size)}>
            <DatePicker
                onDateChange={(val: string): void => {
                    setFocused(true);
                    setModel(moment(val).format('L'));
                    onChange(moment(val).valueOf());
                }}
            />
        </View>
    );

    const itemEL = (
        <Item floatingLabel style={itemStyles(size, disabled)}>
            {labelEL}
            {inputEL}
        </Item>
    );

    const nextEL = next && <Next size={size}>{next}</Next>;

    return (
        <View style={styles.wrapper}>
            {itemEL}
            {datepickerEl}
            {nextEL}
        </View>
    );
}
