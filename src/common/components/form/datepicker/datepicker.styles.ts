import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    wrapper: {
        position: 'relative'
    },
    datepicker: {
        position: 'absolute',
        opacity: 0
    },
    datepickerM: {
        left: 2,
        right: 20,
        top: 8,
        height: 50,
        backgroundColor: '#fff'
    }
});
