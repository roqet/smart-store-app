import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    container: {
        width: 280,
        paddingTop: 40,
        display: 'flex'
    },
    levels: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        marginBottom: 4
    },
    level: {
        fontSize: 18
    },
    progress: {
        height: 16,
        backgroundColor: '$gainsboro',
        borderRadius: 20
    },
    progressbar: {
        height: 16,
        backgroundColor: '$primary',
        borderRadius: 20,
        width: '50%'
    }
});
