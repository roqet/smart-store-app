import React from 'react';
import { ViewStyle } from 'react-native';
import { View, Text } from 'native-base';
import { i18n } from '../../../services';
import { Level, LevelDiscountMap, DiscountLevelMap } from '../../../constants';
import { styles } from './progress.styles';

interface UserProgressProps {
    level: Level;
    progress: number;
}

const LevelsMap: Record<Level, number> = {
    [Level.newcomer]: 1,
    [Level.wiseman]: 2,
    [Level.amateur]: 3,
    [Level.enthusiast]: 4,
    [Level.advanced]: 5,
    [Level.shopaholic]: 6,
    [Level.profі]: 7,
    [Level.onioman]: 8,
    [Level.master]: 9,
    [Level.sensei]: 10,
    [Level.guru]: 11
};

const getProgress = (progress: number): ViewStyle => {
    return {
        width: Math.trunc((280 / 100) * progress)
    };
};

const getCurrentLevel = (level: number): number => {
    if (level === 11) return 10;
    return level;
};

const getNextLevel = (level: number): number => {
    if (level === 12) return 11;
    return level;
};

export function UserProgress({ level, progress }: UserProgressProps): JSX.Element {
    const lvalue = LevelsMap[level];

    return (
        <View style={styles.container}>
            <View style={styles.levels}>
                <Text style={styles.level}>
                    {i18n.t('labels.level', { level: getCurrentLevel(lvalue) })}
                </Text>
                <Text style={styles.level}>
                    {i18n.t('labels.level', { level: getNextLevel(lvalue + 1) })}
                </Text>
            </View>
            <View style={styles.progress}>
                <View style={{ ...styles.progressbar, ...getProgress(progress) }} />
            </View>
        </View>
    );
}
