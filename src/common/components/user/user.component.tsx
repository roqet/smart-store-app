/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable global-require */
import React from 'react';
import { Actions } from 'react-native-router-flux';
import { Image, TouchableOpacity, ImageSourcePropType } from 'react-native';
import { Text, View } from 'native-base';
import { MaleAvatars, FemaleAvatars, Icons, DiscountLevelMap, Level } from '../../constants';
import { i18n } from '../../services';
import { Icon } from '../icon/icon.component';
import { UserProgress } from './progress/progress.component';
import { styles } from './user.styles';

interface UserProps {
    user: Store.User;
}

const logo = require('@/assets/logo.png');

const source = (sex: Store.Sex, level: Level): ImageSourcePropType => {
    if (sex === 0) return MaleAvatars[level];
    if (sex === 1) return FemaleAvatars[level];
    return { uri: '' };
};

export function User({
    user: { lastName, firstName, sex, currentLevel, bonuses, levelPercentage }
}: UserProps): JSX.Element {
    const level = DiscountLevelMap[currentLevel];

    return (
        <View style={styles.wrapper}>
            <View style={styles.top}>
                <View style={styles.circel}>{}</View>
                <Image source={logo} style={styles.logo} />
            </View>
            <View style={styles.info}>
                <TouchableOpacity
                    style={styles.button}
                    onPress={(): void => Actions.push('modal.levels', { gender: sex })}
                >
                    <Image style={styles.image} source={source(sex, level)} />
                    <View style={styles.circle}>
                        <Text style={styles.discount}>{currentLevel}</Text>
                        <Icon name={Icons.percent} style={styles.percent} />
                    </View>
                </TouchableOpacity>
                <Text style={styles.username}>
                    {i18n.t('labels.fullName', { firstName, lastName })}
                </Text>
                <Text style={styles.level}>{i18n.t(`levels.title.${level}`)}</Text>
                <Text style={styles.cashback}>{i18n.t(`labels.cashback`, { value: bonuses })}</Text>
                <UserProgress level={level} progress={levelPercentage} />
            </View>
        </View>
    );
}
