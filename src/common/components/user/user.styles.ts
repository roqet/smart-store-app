import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    wrapper: {
        marginBottom: 119
    },
    top: {
        height: 170,
        backgroundColor: '$primary',
        position: 'relative',
        overflow: 'hidden',
        marginBottom: 34
    },
    circel: {
        width: 780,
        height: 780,
        borderRadius: 390,
        backgroundColor: '$white',
        left: '50%',
        top: 120,
        marginLeft: -390,
        position: 'absolute'
    },
    logo: {
        width: 138,
        height: 75,
        position: 'absolute',
        left: '50%',
        top: 95,
        marginLeft: -69
    },
    info: {
        display: 'flex',
        alignItems: 'center',
        paddingLeft: 20,
        paddingRight: 20
    },
    image: {
        width: 112,
        height: 112
    },
    button: {
        flex: 0,
        marginBottom: 10,
        position: 'relative'
    },
    username: {
        fontSize: 30,
        flex: 0,
        textAlign: 'center',
        marginBottom: 10
    },
    level: {
        fontSize: 18,
        lineHeight: 22
    },
    circle: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: '$primary',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
        right: -10,
        top: -2
    },
    percent: {
        position: 'absolute',
        right: 0,
        top: 5,
        fontSize: 12
    },
    discount: {
        fontSize: 28,
        fontWeight: 'bold'
    },
    cashback: {
        fontSize: 30,
        marginTop: 40
    }
});
