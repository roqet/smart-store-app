import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    header: {
        paddingLeft: 48,
        paddingRight: 48,
        paddingBottom: 32
    },
    title: {
        textAlign: 'center',
        fontSize: 30,
        fontWeight: 'bold'
    },
    note: {
        textAlign: 'center',
        fontSize: 18,
        lineHeight: 22,
        marginTop: 10
    }
});
