import React from 'react';
import { View, Text } from 'native-base';
import { i18n } from '@common/services';
import { styles } from './list.header.styles';

interface ListHeaderProps {
    title?: string;
    note?: string;
}

export function ListHeader({ title, note }: ListHeaderProps): JSX.Element {
    const titleEl = title && <Text style={styles.title}>{title}</Text>;
    const noteEl = note && <Text style={styles.note}>{note}</Text>;
    return (
        <View style={styles.header}>
            {titleEl}
            {noteEl}
        </View>
    );
}
