import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    olLiIndex: {
        color: '$primary',
        fontWeight: '900',
        fontSize: 90,
        position: 'absolute',
        left: -23,
        top: -15
    }
});

export const tags = {
    p: {
        color: '#000000',
        marginBottom: 10,
        fontSize: 14,
        lineHeight: 22
    },
    h2: {
        fontSize: 24,
        lineHeight: 29,
        marginTop: 18,
        marginBottom: 18
    },
    h3: {
        fontSize: 24,
        fontWeight: '400',
        marginBottom: 16,
        color: '#000'
    },
    li: {
        fontSize: 18,
        lineHeight: 26,
        position: 'relative',
        paddingLeft: 10,
        marginTop: 15,
        marginBottom: 15,
        minHeight: 60
    }
};
