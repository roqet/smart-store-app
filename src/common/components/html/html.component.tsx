import merge from 'lodash/merge';
import React from 'react';
import Rederer from 'react-native-render-html';
import { Text } from 'native-base';
import { tags, styles as _styles } from './html.styles';

interface HtmlProps {
    content: string;
    styles?: object;
}

const olLi = (index): JSX.Element => {
    return <Text style={_styles.olLiIndex}>{index}</Text>;
};

const listsPrefixesRenderers = {
    ol: (htmlAttribs, children, convertedCSSStyles, passProps): JSX.Element => {
        return olLi(passProps.index + 1);
    }
};

export function Html({ content, styles = {} }: HtmlProps): JSX.Element {
    return (
        <Rederer
            html={content}
            tagsStyles={merge({}, tags, styles)}
            listsPrefixesRenderers={listsPrefixesRenderers}
        />
    );
}
