import CloudinaryCore from 'cloudinary-core';
import env from 'react-native-config';
import { Dimensions } from 'react-native';

const { Cloudinary } = CloudinaryCore;

const { scale } = Dimensions.get('screen');

const cloudinary = (Cloudinary as any).new({
    cloud_name: env.CLOUDINARY_CLOUD_NAME
});

type Crop = 'thumb';

interface CloudinaryImgProps {
    public_id: string;
    format: string;
    width: number;
    height: number;
    crop?: Crop;
}

export const cloudinaryImg = ({
    // eslint-disable-next-line @typescript-eslint/camelcase
    public_id,
    format,
    width,
    height,
    crop = 'thumb'
}: CloudinaryImgProps): string => {
    return cloudinary.url(public_id, {
        format,
        width: width * scale,
        height: height * scale,
        crop
    });
};
