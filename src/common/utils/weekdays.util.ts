import moment from 'moment';
import capitalize from 'lodash/capitalize';

export const weekdays = {
    0: capitalize(moment.weekdaysShort(1)),
    1: capitalize(moment.weekdaysShort(2)),
    2: capitalize(moment.weekdaysShort(3)),
    3: capitalize(moment.weekdaysShort(4)),
    4: capitalize(moment.weekdaysShort(5)),
    5: capitalize(moment.weekdaysShort(6)),
    6: capitalize(moment.weekdaysShort(0))
};
