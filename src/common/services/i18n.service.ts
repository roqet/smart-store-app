import i18next from 'i18next';
import memoize from 'lodash/memoize';
import { initReactI18next } from 'react-i18next';

interface InitParams {
    defaultLang: string;
    translations: object;
}

class I18n {
    // eslint-disable-next-line class-methods-use-this
    private translate(key: string, options: object = {}): string {
        return i18next.t(key, options);
    }

    // eslint-disable-next-line class-methods-use-this
    private resolver(key: string, options?: object): string {
        if (!options) return key;
        return Object.keys(options).reduce((res: string, option: string): string => {
            return `${res}_${option}_${options[option]}`;
        }, key);
    }

    // eslint-disable-next-line class-methods-use-this
    public init({ defaultLang, translations }: InitParams): void {
        i18next.use(initReactI18next).init({
            lng: defaultLang,
            resources: Object.keys(translations).reduce((res, key): object => {
                return {
                    ...res,
                    [key]: {
                        translation: translations[key]
                    }
                };
            }, {})
        });
    }

    public t: (key: string, options?: object) => string = memoize(this.translate, this.resolver);
}

export const i18n = new I18n();
