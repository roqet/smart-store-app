import { Observable, of } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import AsyncStorage from '@react-native-community/async-storage';

export class Auth {
    public static storeTokenName: string = 'SmartSrore@Token';

    public static saveToken(token: string): Observable<string> {
        return of(token).pipe(
            flatMap(
                (): Promise<string> => {
                    return AsyncStorage.setItem(this.storeTokenName, token).then(
                        (): string => token
                    );
                }
            )
        );
    }

    public static getBearer(): Promise<string> {
        return AsyncStorage.getItem(this.storeTokenName).then((token): string => `Bearer ${token}`);
    }
}
