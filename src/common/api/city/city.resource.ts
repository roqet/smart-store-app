import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { jsonToGraphQLQuery } from 'json-to-graphql-query';
import { City } from './city.queries';
import { Graph } from '../graph';

export class CityResource {
    // eslint-disable-next-line class-methods-use-this
    public all(fields = City): Observable<Store.City[]> {
        return Graph.post('', {
            query: jsonToGraphQLQuery({
                query: {
                    getCities: {
                        records: {
                            ...fields
                        },
                        __args: { count: 9999, page: 0 }
                    }
                }
            })
        }).pipe(map(({ data }): Store.City[] => data.data.getCities.records));
    }
}
