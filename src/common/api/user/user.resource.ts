/* eslint-disable class-methods-use-this */
import { AxiosResponse } from 'axios';
import {} from 'axios-observable';
import {} from 'redux-observable';
import { Observable, EMPTY } from 'rxjs';
import { map, flatMap, catchError } from 'rxjs/operators';
import { jsonToGraphQLQuery } from 'json-to-graphql-query';
import { User } from './user.qureies';
import { Graph } from '../graph';
import { BaseResource } from '../base.resource';

export class UserResource extends BaseResource {
    public current(fields = User): Observable<Store.User> {
        return Graph.post('', {
            query: jsonToGraphQLQuery({
                query: {
                    getCurrentUser: {
                        ...fields
                    }
                }
            })
        }).pipe(map(({ data }): Store.User => data.data.getCurrentUser));
    }

    public create(user: Store.User, fields = User): Observable<Store.User> {
        return Graph.post('', {
            query: jsonToGraphQLQuery({
                mutation: {
                    createUser: {
                        ...fields,
                        __args: {
                            user
                        }
                    }
                }
            })
        }).pipe(map(({ data }): Store.User => data.data.createUser));
    }
}
