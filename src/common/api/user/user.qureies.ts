export const User = {
    id: true,
    phoneNumber: true,
    firstName: true,
    lastName: true,
    dateOfBirth: true,
    sex: true,
    cardNumber: true,
    email: true,
    currentLevel: true,
    bonuses: true,
    levelPercentage: true
};
