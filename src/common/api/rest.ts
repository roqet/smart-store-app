/* eslint-disable prefer-promise-reject-errors */
import { AxiosError } from 'axios';
import get from 'lodash/get';
import env from 'react-native-config';
import camelCase from 'lodash/camelCase';
import Axios from 'axios-observable';

const Rest = Axios.create({
    baseURL: `${env.API_URL}/api/rest`,
    timeout: 60000
});

Rest.interceptors.response.use(
    null,
    (err): Promise<AxiosError> => {
        const error = get(err, 'response.data.error_message');
        return Promise.reject({
            errors: [error ? `errors.${camelCase(error)}` : 'errors.common']
        });
    }
);

export { Rest };
