import { Cloudinary } from '../cloudinaty.query';

export const Promotion = {
    id: true,
    title: true,
    type: true,
    content: true,
    createdAt: true,
    image: Cloudinary
};
