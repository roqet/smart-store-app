/* eslint-disable class-methods-use-this */
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { jsonToGraphQLQuery } from 'json-to-graphql-query';
import { Graph } from '../graph';
import { Promotion } from './promotions.queries';

export class PromotionsResource {
    public query(
        params: Store.Api.PaginationParams,
        fields = Promotion
    ): Observable<Store.Api.PagedResponse<Store.Promotion>> {
        return Graph.post('', {
            query: jsonToGraphQLQuery({
                query: {
                    getAllNews: {
                        total: true,
                        records: fields,
                        __args: params
                    }
                }
            })
        }).pipe(map(({ data }): Store.Api.PagedResponse<Store.Promotion> => data.data.getAllNews));
    }

    public get(id: number, fields = Promotion): Observable<Store.Promotion> {
        return Graph.post('', {
            query: jsonToGraphQLQuery({
                query: {
                    getNews: {
                        ...fields,
                        __args: { id }
                    }
                }
            })
        }).pipe(map(({ data }): Store.Promotion => data.data.getNews));
    }
}
