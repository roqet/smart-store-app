/* eslint-disable prefer-promise-reject-errors */
import merge from 'lodash/merge';
import env from 'react-native-config';
import { AxiosRequestConfig, AxiosResponse, AxiosError } from 'axios';
import Axios from 'axios-observable';
import { Auth } from '../services/auth.service';
console.log(env)
const Graph = Axios.create({
    timeout: 60000,
    baseURL: `${env.API_URL}/graphql`
});

Graph.interceptors.request.use(
    (options): Promise<AxiosRequestConfig> => {
        return Auth.getBearer().then(
            (bearer): AxiosRequestConfig => {
                return merge({}, options, {
                    headers: {
                        Authorization: bearer
                    }
                });
            }
        );
    }
);

Graph.interceptors.response.use(
    (response): Promise<AxiosResponse> => {
        const { errors } = response.data;
        if (errors && errors.length) {
            return Promise.reject({
                errors: errors.map(({ message }): string => message)
            });
        }
        return Promise.resolve(response);
    },
    (error): Promise<AxiosError> => {
        return Promise.reject({
            errors: ['errors.common']
        });
    }
);

export { Graph };
