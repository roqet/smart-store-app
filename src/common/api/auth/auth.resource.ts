/* eslint-disable class-methods-use-this */
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Rest } from '../rest';

export class AuthResource {
    public sendPassword(tel: string): Observable<string> {
        return Rest.post('/sendPassword', tel).pipe(map((): string => tel));
    }

    public logout(): Observable<void> {
        return Rest.post('/logout').pipe(map((): void => {}));
    }

    public login(creds: object): Observable<string> {
        return Rest.post('/login', creds).pipe(map(({ data }): string => data));
    }
}
