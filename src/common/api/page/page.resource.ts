import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { jsonToGraphQLQuery } from 'json-to-graphql-query';
import { Page, Pages } from './page.queries';
import { Graph } from '../graph';

export class PageResource {
    // eslint-disable-next-line class-methods-use-this
    public get<T>(type: Pages, fields?): Observable<T> {
        return Graph.post('', {
            query: jsonToGraphQLQuery({
                query: {
                    getPage: {
                        ...(fields || Page[type]),
                        __args: { type }
                    }
                }
            })
        }).pipe(map(({ data }): T => data.data.getPage));
    }
}
