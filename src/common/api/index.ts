import { PromotionsResource } from './promotions/promotions.resource';
import { PageResource } from './page/page.resource';
import { Pages } from './page/page.queries';
import { CityResource } from './city/city.resource';
import { ShopResource } from './shop/shop.resource';
import { AuthResource } from './auth/auth.resource';
import { UserResource } from './user/user.resource';

export class SmartApi {
    public static promotions: PromotionsResource = new PromotionsResource();
    public static page: PageResource = new PageResource();
    public static city: CityResource = new CityResource();
    public static shop: ShopResource = new ShopResource();
    public static auth: AuthResource = new AuthResource();
    public static user: UserResource = new UserResource();
}

export { Pages };
