import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { jsonToGraphQLQuery } from 'json-to-graphql-query';
import { Shop } from './shop.queries';
import { Graph } from '../graph';

export class ShopResource {
    // eslint-disable-next-line class-methods-use-this
    public all(id: number, fields: any = Shop): Observable<Store.Shop[]> {
        return Graph.post('', {
            query: jsonToGraphQLQuery({
                query: {
                    getMarkets: {
                        records: {
                            ...fields
                        },
                        __args: { cityId: Number(id), count: 9999, page: 0 }
                    }
                }
            })
        }).pipe(map(({ data }): Store.Shop[] => data.data.getMarkets.records));
    }

    // eslint-disable-next-line class-methods-use-this
    public get(id: number, fields: any = Shop): Observable<Store.Shop> {
        return Graph.post('', {
            query: jsonToGraphQLQuery({
                query: {
                    getMarketWithDetails: {
                        ...fields,
                        __args: { marketId: Number(id) }
                    }
                }
            })
        }).pipe(map(({ data }): Store.Shop => data.data.getMarketWithDetails));
    }
}
