export const Shop = {
    id: true,
    address: {
        id: true,
        name: true,
        cityId: true
    },
    workingSchedule: {
        dayNum: true,
        start: true,
        end: true,
        isClosed: true
    },
    prices: {
        deliveryPrices: true,
        luxuryPrices: true
    },
    dayOfDelivery: true,
    deliveryLux: true,
    geometry: {
        latitude: true,
        longitude: true
    }
};
