/* eslint-disable class-methods-use-this */
import { AsyncStorage } from 'react-native';
import { Observable, of } from 'rxjs';
import { flatMap } from 'rxjs/operators';

interface CookieHeader {
    Cookie: string;
    [key: string]: string;
}

export class BaseResource {
    public getJsSessionId(): Observable<CookieHeader> {
        return of(1).pipe(
            flatMap(
                (): Promise<CookieHeader> =>
                    AsyncStorage.getItem('SmartSrore@JSESSIONID').then(
                        (id): CookieHeader => ({
                            // Accept: 'application/json',
                            // 'Accept-Encoding': 'gzip, deflate',
                            // 'Accept-Language': 'en-US,en;q=0.9,ru;q=0.8',
                            // 'Cache-Control': 'no-cache',
                            // Connection: 'keep-alive',
                            // 'Content-Length': '261',
                            // 'Content-Type': 'application/json',
                            Cookie: `JSESSIONID=${id}`
                            // Host: 'smart-store.us-east-1.elasticbeanstalk.com',
                            // Origin: 'http://smart-store.us-east-1.elasticbeanstalk.com',
                            // Pragma: 'no-cache',
                            // Referer: 'http://smart-store.us-east-1.elasticbeanstalk.com/',
                            // 'User-Agent':
                            //     'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36'
                        })
                    )
            )
        );
    }
}
