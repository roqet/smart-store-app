import { Level } from './level.const';

export const LevelDiscountMap: Record<Level, number> = {
    [Level.newcomer]: 3,
    [Level.wiseman]: 4,
    [Level.amateur]: 5,
    [Level.enthusiast]: 6,
    [Level.advanced]: 7,
    [Level.shopaholic]: 8,
    [Level.profі]: 9,
    [Level.onioman]: 10,
    [Level.master]: 11,
    [Level.sensei]: 12,
    [Level.guru]: 13
};
