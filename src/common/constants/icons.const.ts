export enum Icons {
    contact = 'contact',
    facebook = 'facebook',
    gift = 'gift',
    home = 'home',
    instagram = 'instagram',
    location = 'location',
    team = 'team',
    telegram = 'telegram',
    call = 'call-answer',
    x = 'x',
    checkmark = 'checkmark',
    polygon = 'polygon',
    marker = 'marker',
    percent = 'percent',
    edit = 'edit',
    barcode = 'barcode'
}
