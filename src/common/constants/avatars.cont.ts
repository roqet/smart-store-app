/* eslint-disable global-require */
import { ImageSourcePropType } from 'react-native';
import { Level } from './level.const';

export const MaleAvatars: Record<Level, ImageSourcePropType> = {
    newcomer: require('../../assets/levels/male/newcomer.png'),
    wiseman: require('../../assets/levels/male/wiseman.png'),
    amateur: require('../../assets/levels/male/amateur.png'),
    enthusiast: require('../../assets/levels/male/enthusiast.png'),
    advanced: require('../../assets/levels/male/advanced.png'),
    shopaholic: require('../../assets/levels/male/shopaholic.png'),
    profі: require('../../assets/levels/male/profі.png'),
    onioman: require('../../assets/levels/male/onioman.png'),
    master: require('../../assets/levels/male/master.png'),
    sensei: require('../../assets/levels/male/sensei.png'),
    guru: require('../../assets/levels/male/guru.png')
};

export const FemaleAvatars: Record<Level, ImageSourcePropType> = {
    newcomer: require('../../assets/levels/female/newcomer.png'),
    wiseman: require('../../assets/levels/female/wiseman.png'),
    amateur: require('../../assets/levels/female/amateur.png'),
    enthusiast: require('../../assets/levels/female/enthusiast.png'),
    advanced: require('../../assets/levels/female/advanced.png'),
    shopaholic: require('../../assets/levels/female/shopaholic.png'),
    profі: require('../../assets/levels/female/profі.png'),
    onioman: require('../../assets/levels/female/onioman.png'),
    master: require('../../assets/levels/female/master.png'),
    sensei: require('../../assets/levels/female/sensei.png'),
    guru: require('../../assets/levels/female/guru.png')
};
