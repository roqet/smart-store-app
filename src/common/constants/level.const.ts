export enum Level {
    newcomer = 'newcomer',
    wiseman = 'wiseman',
    amateur = 'amateur',
    enthusiast = 'enthusiast',
    advanced = 'advanced',
    shopaholic = 'shopaholic',
    profі = 'profі',
    onioman = 'onioman',
    master = 'master',
    sensei = 'sensei',
    guru = 'guru'
}
