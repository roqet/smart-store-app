import { Level } from './level.const';

export const DiscountLevelMap: Record<number, Level> = {
    3: Level.newcomer,
    4: Level.wiseman,
    5: Level.amateur,
    6: Level.enthusiast,
    7: Level.advanced,
    8: Level.shopaholic,
    9: Level.profі,
    10: Level.onioman,
    11: Level.master,
    12: Level.sensei,
    13: Level.guru
};
