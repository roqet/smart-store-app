// @flow

import { Platform } from 'react-native';

import variable from '../variables/platform';

export default (variables /* : * */ = variable) => {
    const titleTheme = {
        fontSize: 14,
        fontFamily: variables.titleFontfamily,
        color: '#000',
        fontWeight: '400',
        textAlign: 'center',
        paddingLeft: Platform.OS === 'ios' ? 4 : 0,
        marginLeft: Platform.OS === 'ios' ? undefined : -3,
        paddingTop: 1
    };

    return titleTheme;
};
