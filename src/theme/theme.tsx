import React, { PropsWithChildren } from 'react';
import { StyleProvider } from 'native-base';
import theme from '@theme/components';
import smartstore from '@theme/variables/smartstore';

export function Theme({ children }: PropsWithChildren<{}>): JSX.Element {
    return <StyleProvider style={theme(smartstore)}>{children}</StyleProvider>;
}
