import { connect } from 'react-redux';
import { RootSelectors, RootActions } from '@/store/root';
import { Screens, ScreensProps } from './screens.component';

const mapStateToProps = (state: AppState): Partial<ScreensProps> => ({
    user: RootSelectors.user(state),
    initialized: RootSelectors.initialized(state),
    errors: RootSelectors.errors(state)
});

const mapDispatchToProps = (dispatch): Partial<ScreensProps> => ({
    init: (): void => dispatch(new RootActions.Init())
});

export const ScreensContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Screens);
