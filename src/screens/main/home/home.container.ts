import { connect } from 'react-redux';
import { RootSelectors } from '@/store/root';
import { Home, HomeProps } from './home.component';

const mapStateToProps = (state: AppState): Partial<HomeProps> => ({
    user: RootSelectors.user(state)
});

export const HomeContainer = connect(mapStateToProps)(Home);
