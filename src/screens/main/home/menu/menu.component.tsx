import React from 'react';
import { TouchableOpacity } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { View, Text, Button } from 'native-base';
import { Icons } from '@common/constants';
import { i18n } from '@common/services';
import { Icon } from '@common/components';
import { styles } from './menu.styles';

interface MenuItem {
    icon: Icons;
    label: string;
    size: number;
    left?: number;
    top?: number;
    nav: string;
}

const row = (menu: MenuItem[], key: number): JSX.Element => {
    return (
        <View style={styles.row} key={key}>
            {menu.map(
                ({ icon, label, size, left, top, nav }, index): JSX.Element => {
                    return (
                        <View
                            key={icon}
                            style={{
                                ...styles.item,
                                ...(index === 0 ? { marginRight: 68 } : { marginLeft: 68 })
                            }}
                        >
                            <TouchableOpacity
                                style={styles.button}
                                onPress={(): void => Actions.push(nav)}
                            >
                                <Icon
                                    name={icon}
                                    style={{
                                        ...styles.icon,
                                        fontSize: size,
                                        marginLeft: left,
                                        marginTop: top
                                    }}
                                />
                            </TouchableOpacity>
                            <Text style={styles.label}>{label}</Text>
                        </View>
                    );
                }
            )}
        </View>
    );
};

export function HomeMenu(): JSX.Element {
    const firstRow: MenuItem[] = [
        {
            icon: Icons.gift,
            label: i18n.t('pages.promotions'),
            size: 54,
            left: 8,
            nav: 'main.promotions'
        },
        {
            icon: Icons.location,
            label: i18n.t('pages.shops'),
            size: 40,
            nav: 'main.shops'
        }
    ];

    const secondRow: MenuItem[] = [
        {
            icon: Icons.team,
            label: i18n.t('pages.aboutUs'),
            size: 56,
            nav: 'main.about'
        },
        {
            icon: Icons.contact,
            label: i18n.t('pages.contacts'),
            size: 40,
            left: -4,
            top: 6,
            nav: 'main.contacts'
        }
    ];

    return (
        <View style={styles.menu}>
            {[firstRow, secondRow].map(
                (menuRow, index): JSX.Element => {
                    return row(menuRow, index);
                }
            )}
        </View>
    );
}
