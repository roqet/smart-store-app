import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    label: {
        fontSize: 18
    },
    icon: {
        position: 'relative',
        zIndex: 2,
        overflow: 'visible'
    },
    button: {
        width: 60,
        height: 60,
        backgroundColor: '$primary',
        borderRadius: 30,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 5,
        overflow: 'visible',
        position: 'relative',
        zIndex: 1
    },
    menu: {
        width: '100%',
        display: 'flex'
    },
    row: {
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        marginBottom: 53
    },
    item: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        overflow: 'visible',
        zIndex: 1
    }
});
