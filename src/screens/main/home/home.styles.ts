import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    content: {
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: -1,
        flex: 1
    },
    action: {
        fontSize: 24
    },
    headerLeft: {
        flex: 0
    },
    user: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    barcode: {
        position: 'absolute',
        left: '50%',
        bottom: 64,
        zIndex: 2,
        width: 86,
        height: 86,
        marginLeft: -43,
        borderRadius: 43,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        color: '$white',
        borderWidth: 1,
        borderColor: '$white',
        paddingVertical: 2,
        paddingHorizontal: 4,
        borderRadius: 5,
        fontSize: 30
    }
});
