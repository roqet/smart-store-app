/* eslint-disable no-undef */
import React from 'react';
import { Actions } from 'react-native-router-flux';
import { ScrollView } from 'react-native';
import { Container, Header, Left, Button, Body, Right, View } from 'native-base';
import { User, Icon } from '@common/components';
import { Icons } from '@common/constants';
import { HomeMenu } from './menu/menu.component';
import { styles } from './home.styles';

export function Home({ user }: HomeProps): JSX.Element {
    const leftAction = (
        <Left style={styles.headerLeft}>
            <Button transparent onPress={(): void => Actions.push('main.profile')}>
                <Icon name='user' style={styles.action} />
            </Button>
        </Left>
    );

    const rightAction = __DEV__ && (
        <Right>
            <Button transparent onPress={(): void => Actions.push('main.notifications')}>
                <Icon name='bell' style={styles.action} />
            </Button>
        </Right>
    );

    const barcodeAction = (
        <Button dark style={styles.barcode} onPress={(): void => Actions.push('modal.card')}>
            <Icon name={Icons.barcode} style={styles.icon} />
        </Button>
    );

    return (
        <Container style={styles.container}>
            <Header noShadow iosBarStyle='dark-content' transparent style={styles.header}>
                {leftAction}
                <Body />
                {rightAction}
            </Header>
            <View style={styles.content}>
                <ScrollView
                    style={styles.scroll}
                    bounces={false}
                    showsVerticalScrollIndicator={false}
                >
                    <User user={user} />
                    <HomeMenu />
                </ScrollView>
                {barcodeAction}
            </View>
        </Container>
    );
}

export interface HomeProps {
    user: Store.User & {};
}
