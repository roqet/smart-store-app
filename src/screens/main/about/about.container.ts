import { connect } from 'react-redux';
import { About, AboutProps } from './about.component';
import { AboutSelectors, AboutActions } from '@/store/about';

const mapStateToProps = (state: AppState): Partial<AboutProps> => ({
    loading: AboutSelectors.loading(state),
    initialized: AboutSelectors.initialized(state),
    about: AboutSelectors.about(state)
});

const mapDispatchToProps = (dispatch): Partial<AboutProps> => ({
    load: (): void => dispatch(new AboutActions.LoadAbout())
});

export const AboutContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(About as any);
