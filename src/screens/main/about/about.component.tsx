import React, { useEffect } from 'react';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Left, Button, Body, Title, Right, View } from 'native-base';
import { i18n } from '@common/services';
import { Loader, Icon, Html } from '@common/components';
import { styles } from './about.styles';

export function About({ initialized, about, load, loading }: AboutProps): JSX.Element {
    useEffect((): void => {
        if (!initialized) {
            load();
        }
    }, [initialized, load]);

    const loader: JSX.Element = <Loader show={loading} size='large' />;

    const content: JSX.Element = (
        <View style={styles.content}>
            <Html content={about.content} />
        </View>
    );

    return (
        <Container>
            <Header noShadow iosBarStyle='dark-content' translucent={false}>
                <Left style={{ flex: 0 }}>
                    <Button transparent onPress={(): void => Actions.pop()}>
                        <Icon name='chevron-left' />
                    </Button>
                </Left>
                <Body>
                    <Title>{i18n.t('pages.aboutUs')}</Title>
                </Body>
                <Right />
            </Header>
            <Content>{!initialized ? loader : content}</Content>
        </Container>
    );
}

export interface AboutProps {
    loading: boolean;
    initialized: boolean;
    about: Store.About;
    load: () => void;
}
