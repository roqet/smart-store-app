import React from 'react';
import { TouchableOpacity, ScrollView } from 'react-native';
import { Actions } from 'react-native-router-flux';
import {
    Container,
    Header,
    Content,
    Left,
    Button,
    Body,
    Title,
    Right,
    View,
    Text
} from 'native-base';
import Barcode from 'react-native-barcode-builder';
import { User, Icon } from '@common/components';
import { i18n } from '@common/services';
import { styles } from './profile.styles';

export function Profile({ user }: ProfileProps): JSX.Element {
    const content = (
        <View style={{ flex: 1 }}>
            <User user={user} />
        </View>
    );

    const barcode = (
        <TouchableOpacity style={styles.barcode} onPress={(): void => Actions.push('modal.card')}>
            <Text style={styles.barcodeText}>{i18n.t('buttons.customerCard').toUpperCase()}</Text>
            <Barcode
                value={user.cardNumber}
                background='#000000'
                lineColor='#ffffff'
                height={80}
                width={3}
                format='EAN13'
                flat
            />
        </TouchableOpacity>
    );

    return (
        <Container>
            <Header noShadow transparent iosBarStyle='dark-content'>
                <Left style={{ flex: 0 }}>
                    <Button transparent onPress={(): void => Actions.pop()}>
                        <Icon name='chevron-left' />
                    </Button>
                </Left>
                <Body>
                    <Title>{i18n.t('pages.personalOffice')}</Title>
                </Body>
                <Right />
            </Header>
            <View style={styles.content}>
                <ScrollView style={styles.scroll} bounces={false}>
                    {content}
                </ScrollView>
                {barcode}
            </View>
        </Container>
    );
}

export interface ProfileProps {
    user: Store.User;
}
