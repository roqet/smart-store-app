import { connect } from 'react-redux';
import { Profile, ProfileProps } from './profile.component';
import { RootSelectors } from '@/store/root';

const mapStateToProps = (state: AppState): Partial<ProfileProps> => ({
    user: RootSelectors.user(state)
});

export const ProfileContainer = connect(mapStateToProps)(Profile);
