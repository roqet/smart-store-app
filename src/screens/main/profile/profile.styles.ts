import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 0
    },
    content: {
        flex: 1,
        padding: 0,
        position: 'absolute',
        width: '100%',
        height: '100%',
        zIndex: -1
    },
    scroll: {
        flex: 1
    },
    barcode: {
        backgroundColor: '$black',
        borderTopRightRadius: 25,
        borderTopLeftRadius: 25,
        display: 'flex',
        alignItems: 'center'
    },
    barcodeText: {
        color: '$primary',
        fontSize: 14,
        fontWeight: 'bold',
        marginTop: 16,
        marginBottom: 8
    }
});
