import { connect } from 'react-redux';
import { PromotionActions, PromotionSelectors } from '@/store/promotion';
import { Promotion, PromotionProps } from './promotion.component';

const mapStateToProps = (state: AppState, props: PromotionProps): Partial<PromotionProps> => ({
    id: props.id,
    loading: PromotionSelectors.loading(state),
    initialized: PromotionSelectors.initialized(state),
    promotion: PromotionSelectors.promotion(state)
});

const mapDispatchToProps = (dispatch): Partial<PromotionProps> => ({
    load: (id: number): void => dispatch(new PromotionActions.LoadPromotion(id)),
    initialize: (initialized: boolean): void =>
        dispatch(new PromotionActions.Initialize(initialized))
});

export const PromotionContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Promotion as any);
