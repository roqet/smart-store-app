/* eslint-disable react-hooks/exhaustive-deps */
import moment from 'moment';
import React, { useEffect, EffectCallback } from 'react';
import { Actions } from 'react-native-router-flux';
import {
    View,
    Container,
    Header,
    Button,
    Left,
    Body,
    Right,
    Title,
    Content,
    Text
} from 'native-base';
import { i18n } from '@common/services';
import { Icon, Separator, Html, Loader } from '@common/components';
import { PromotionImage } from './image/image.component';
import { styles, tags } from './promotion.styles';

export interface PromotionProps {
    id: number;
    loading: boolean;
    initialized: boolean;
    promotion: Store.Promotion;
    load: (id: number) => void;
    initialize: (initialized: boolean) => void;
}

export function Promotion({
    id,
    load,
    promotion,
    loading,
    initialized,
    initialize
}: PromotionProps): JSX.Element {
    useEffect((): EffectCallback => {
        if (!promotion || promotion.id !== id) load(id);
        else initialize(true);
        return (): void => initialize(false);
    }, [initialize, load]);

    const loader: JSX.Element = (loading || !initialized) && <Loader show={loading} size='large' />;

    const content: JSX.Element = !loading && initialized && (
        <View>
            <View style={styles.top}>
                <Text style={styles.note}>
                    {moment().format('L')}
                    {'     '}
                    {i18n.t(`forms.${promotion.type.toLocaleLowerCase()}`)}
                </Text>
                <PromotionImage {...promotion.image} />
                <Text style={styles.title}>{promotion.title}</Text>
            </View>
            <View style={styles.separator}>
                <Separator />
            </View>
            <View style={styles.content}>
                <Html content={promotion.content} styles={tags} />
            </View>
        </View>
    );

    return (
        <Container>
            <Header noShadow iosBarStyle='dark-content' translucent={false}>
                <Left style={{ flex: 0 }}>
                    <Button transparent onPress={(): void => Actions.pop()}>
                        <Icon name='chevron-left' />
                    </Button>
                </Left>
                <Body>
                    <Title>{i18n.t('pages.newsSlashPromotions')}</Title>
                </Body>
                <Right />
            </Header>
            <Content>
                {loader}
                {content}
            </Content>
        </Container>
    );
}
