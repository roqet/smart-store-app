/* eslint-disable @typescript-eslint/camelcase */
import React from 'react';
import { Image, Dimensions } from 'react-native';
import { cloudinaryImg } from '@common/utils';

const { scale, width } = Dimensions.get('screen');
const imgWidth = width - 40;
const imgHeight = Math.floor(imgWidth * 0.6);

export function PromotionImage({
    public_id = '',
    format = ''
}: Partial<Store.CloudinaryImage>): JSX.Element {
    return (
        <Image
            style={{ width: imgWidth, height: imgHeight }}
            source={{
                uri: cloudinaryImg({
                    public_id,
                    format,
                    width: imgWidth * scale,
                    height: imgHeight * scale
                })
            }}
        />
    );
}
