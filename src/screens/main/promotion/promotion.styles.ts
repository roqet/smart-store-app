import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    top: {
        display: 'flex',
        flexDirection: 'column',
        width: '100%',
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20
    },
    separator: {
        marginTop: 20
    },
    note: {
        fontSize: 14,
        lineHeight: 14,
        marginBottom: 14,
        color: '$dustyGray'
    },
    title: {
        fontSize: 24,
        lineHeight: 30,
        fontWeight: 'bold',
        marginTop: 17,
        textAlign: 'center'
    },
    content: {
        padding: 20
    },
    loader: {
        flex: 1,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }
});

export const tags = StyleSheet.create({
    p: {
        color: '$tundora'
    }
});
