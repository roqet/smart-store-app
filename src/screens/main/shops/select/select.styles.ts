import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    content: {
        paddingBottom: 27
    },
    label: {
        fontSize: 18,
        lineHeight: 22,
        marginBottom: 14,
        textAlign: 'center'
    },
    button: {
        marginBottom: 12
    },
    text: {
        color: '$doveGray',
        flex: 1,
        textAlign: 'center'
    },
    icon: {
        fontSize: 7,
        marginRight: 6
    }
});
