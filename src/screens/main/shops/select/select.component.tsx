import React from 'react';
import { View, Text, Button } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { i18n } from '@common/services';
import { Icon } from '@common/components';
import { Icons } from '@common/constants';
import { styles } from './select.styles';

interface ShopsSelectProps {
    cities: Store.City[];
    cityId: number;
}

const city = (cities: Store.City[], cityId: number): string => {
    return (
        cities.find(({ id }): boolean => {
            return id === cityId;
        }) || { name: i18n.t('buttons.chooseCity') }
    ).name.toUpperCase();
};

export function ShopsSelect({ cities, cityId }: ShopsSelectProps): JSX.Element {
    return (
        <View style={styles.content}>
            <Text style={styles.label}>{i18n.t('forms.cityShops')}</Text>
            <Button
                bordered
                dark
                full
                style={styles.button}
                onPress={(): void => Actions.push('cities')}
            >
                <Text style={styles.text}>{city(cities, cityId)}</Text>
                <Icon name={Icons.polygon} style={styles.icon} />
            </Button>
        </View>
    );
}
