import React, { useEffect } from 'react';
import { Actions } from 'react-native-router-flux';
import {
    Container,
    Header,
    Left,
    Button,
    Body,
    Title,
    Content,
    Right,
    View,
    Footer
} from 'native-base';
import { i18n } from '@common/services';
import { Icon, Loader, Legend } from '@common/components';
import { ShopsList } from './list/list.component';
import { ShopsSelect } from './select/select.component';
import { styles } from './shops.styles';

export function Shops({
    loading,
    initialized,
    init,
    cities,
    cityId,
    shops
}: ShopsProps): JSX.Element {
    useEffect((): void => {
        if (!initialized) init();
    }, [init, initialized]);

    const loader = !initialized && <Loader show size='large' />;

    const content = initialized && (
        <View style={styles.content}>
            <ShopsSelect cities={cities} cityId={cityId} />
            <ShopsList shops={shops} loading={loading} />
        </View>
    );

    const footer = initialized && (
        <Footer style={styles.footer}>
            <Legend />
        </Footer>
    );

    return (
        <Container>
            <Header noShadow iosBarStyle='dark-content' translucent={false}>
                <Left style={{ flex: 0 }}>
                    <Button transparent onPress={(): void => Actions.pop()}>
                        <Icon name='chevron-left' />
                    </Button>
                </Left>
                <Body>
                    <Title>{i18n.t('pages.shops')}</Title>
                </Body>
                <Right />
            </Header>
            <Content>
                {loader}
                {content}
            </Content>
            {footer}
        </Container>
    );
}

export interface ShopsProps {
    loading: boolean;
    initialized: boolean;
    cityId: number;
    cities: Store.City[];
    shops: Store.Shop[];
    init: () => void;
}
