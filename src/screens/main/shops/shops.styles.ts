import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    content: {
        paddingTop: 39,
        paddingLeft: 47,
        paddingRight: 47
    },
    footer: {
        display: 'flex',
        flexDirection: 'row',
        paddingLeft: 20,
        paddingRight: 20,
        alignItems: 'center'
    },
    label: {
        fontSize: 18,
        lineHeight: 22,
        marginBottom: 14,
        textAlign: 'center'
    },
    button: {
        marginBottom: 12
    },
    btnText: {
        color: '$doveGray'
    }
});
