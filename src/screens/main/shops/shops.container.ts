import { connect } from 'react-redux';
import { Shops, ShopsProps } from './shops.component';
import { ShopsSelectors, ShopsActions } from '@/store/shops';

const mapStateToProps = (state: AppState): Partial<ShopsProps> => ({
    loading: ShopsSelectors.loading(state),
    initialized: ShopsSelectors.initialized(state),
    cities: ShopsSelectors.cities(state),
    shops: ShopsSelectors.shops(state),
    cityId: ShopsSelectors.cityId(state)
});

const mapDispatchToProps = (dispatch): Partial<ShopsProps> => ({
    init: (): void => dispatch(new ShopsActions.Initialize())
});

export const ShopsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Shops as any);
