import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    button: {
        marginTop: 12,
        marginBottom: 12
    },
    btnText: {
        fontSize: 14
    },
    btnDelivery: {
        backgroundColor: '#000'
    },
    btnDeliveryText: {
        color: '#fff'
    },
    btnLux: {
        backgroundColor: '$primary'
    }
});
