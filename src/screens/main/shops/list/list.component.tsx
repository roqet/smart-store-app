import React from 'react';
import moment from 'moment';
import { Actions } from 'react-native-router-flux';
import { Loader } from '@common/components';
import { Button, Text, View } from 'native-base';
import { styles } from './list.styles';

interface ShopsListProps {
    loading: boolean;
    shops: Store.Shop[];
}

export function ShopsList({ loading, shops }: ShopsListProps): JSX.Element {
    const loader = <Loader show size='large' />;

    const today = moment().weekday();

    const content = (
        <View style={styles.container}>
            {shops.map(
                ({ id, dayOfDelivery, deliveryLux, address: { name } }): JSX.Element => {
                    return (
                        <Button
                            full
                            bordered={today !== deliveryLux && today !== dayOfDelivery}
                            light={today !== deliveryLux && today !== dayOfDelivery}
                            dark={today === dayOfDelivery}
                            primary={today === deliveryLux}
                            style={styles.button}
                            key={id}
                            onPress={(): void => Actions.push('main.shop', { id })}
                        >
                            <Text>{name}</Text>
                        </Button>
                    );
                }
            )}
        </View>
    );

    return loading ? loader : content;
}
