import React from 'react';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Left, Button, Body, Title, Right, View } from 'native-base';
import { i18n } from '@common/services';
import { Icon } from '@common/components';

export function Notifications({ loading }: NotificationsProps): JSX.Element {
    return (
        <Container>
            <Header noShadow iosBarStyle='dark-content' translucent={false}>
                <Left style={{ flex: 0 }}>
                    <Button transparent onPress={(): void => Actions.pop()}>
                        <Icon name='chevron-left' />
                    </Button>
                </Left>
                <Body>
                    <Title>{i18n.t('pages.notifications')}</Title>
                </Body>
                <Right />
            </Header>
            <Content>{loading ? null : null}</Content>
        </Container>
    );
}

export interface NotificationsProps {
    loading: boolean;
}
