import { connect } from 'react-redux';
import { Notifications, NotificationsProps } from './notifications.component';

const mapStateToProps = (state: AppState): Partial<NotificationsProps> => ({
    loading: false
});

const mapDispatchToProps = (dispatch): Partial<NotificationsProps> => ({});

export const NotificationsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Notifications as any);
