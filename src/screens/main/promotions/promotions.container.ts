import { connect } from 'react-redux';
import { PromotionsActions, PromotionsSelectors } from '@/store/promotions';
import { Promotions, PromotionsProps } from './promotions.component';

const mapStateToProps = (state: AppState): Partial<PromotionsProps> => ({
    loading: PromotionsSelectors.loading(state),
    refreshing: PromotionsSelectors.refreshing(state),
    data: PromotionsSelectors.data(state)
});

const mapDispatchToProps = (dispatch): Partial<PromotionsProps> => ({
    load: (): void => dispatch(new PromotionsActions.LoadPromotions()),
    refresh: (): void => dispatch(new PromotionsActions.RefreshPromotions())
});

export const PromotionsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Promotions as any);
