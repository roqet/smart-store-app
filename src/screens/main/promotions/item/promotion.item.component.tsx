import React from 'react';
import moment from 'moment';
import { Image } from 'react-native';
import { ListItem, Left, Body, Text } from 'native-base';
import { cloudinaryImg } from '@common/utils';
import { i18n } from '@common/services';
import { styles } from './promotion.item.styles';

export interface PromotionItemProps {
    promotion: Store.Promotion;
    onPress: (promotion: Store.Promotion) => void;
}

export function PromotionItem({ promotion, onPress }: PromotionItemProps): JSX.Element {
    return (
        <ListItem avatar style={styles.item} noBorder onPress={(): void => onPress(promotion)}>
            <Left style={styles.left}>
                <Image
                    style={styles.thumb}
                    source={{
                        uri: cloudinaryImg({
                            public_id: promotion.image.public_id || '',
                            width: 98,
                            height: 60,
                            format: promotion.image.format || ''
                        })
                    }}
                />
            </Left>
            <Body style={styles.body}>
                <Text style={styles.note}>
                    {moment(promotion.createdAt).format('L')}
                    {'     '}
                    {i18n.t(`forms.${promotion.type.toLocaleLowerCase()}`)}
                </Text>
                <Text numberOfLines={2} style={styles.title}>
                    {promotion.title}
                </Text>
            </Body>
        </ListItem>
    );
}
