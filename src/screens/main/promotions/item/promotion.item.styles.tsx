import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    item: {
        paddingRight: 20,
        paddingLeft: 20,
        paddingTop: 16,
        paddingBottom: 16,
        marginLeft: 0
    },
    left: {
        paddingTop: 0
    },
    thumb: {
        width: 98,
        height: 60
    },
    body: {
        marginLeft: 0,
        paddingLeft: 14,
        paddingTop: 0,
        paddingBottom: 0,
        alignSelf: 'flex-start'
    },
    title: {
        marginRight: 0,
        fontSize: 18,
        fontWeight: 'bold',
        lineHeight: 22
    },
    note: {
        fontSize: 12,
        lineHeight: 12,
        marginBottom: 5,
        color: '$dustyGray'
    }
});
