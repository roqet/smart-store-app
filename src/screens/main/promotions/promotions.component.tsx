import React, { useEffect } from 'react';
import { Container, Header, Left, Button, Body, Right, Title, View } from 'native-base';
import { FlatList } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Separator, Loader, Icon } from '@common/components';
import { i18n } from '@common/services';
import { PromotionItem } from './item/promotion.item.component';

export function Promotions({
    load,
    data,
    refresh,
    refreshing,
    loading
}: PromotionsProps): JSX.Element {
    useEffect((): void => {
        load();
    }, [load]);

    const promotionItem = ({ item }: any): JSX.Element => (
        <PromotionItem
            promotion={item}
            onPress={({ id }): void => Actions.push('main.promotion', { id })}
        />
    );

    const loader = (): JSX.Element => <Loader show={loading} size='large' />;

    const onEndReached = (): void => {
        if (data.total > data.records.length && !loading) load();
    };

    const keyExtractor = (item): string => item.id.toString();

    return (
        <Container>
            <Header noShadow iosBarStyle='dark-content' translucent={false}>
                <Left style={{ flex: 0 }}>
                    <Button transparent onPress={(): void => Actions.pop()}>
                        <Icon name='chevron-left' />
                    </Button>
                </Left>
                <Body>
                    <Title>{i18n.t('pages.newsSlashPromotions')}</Title>
                </Body>
                <Right />
            </Header>
            <View style={{ flex: 1 }}>
                <FlatList
                    data={data.records}
                    onRefresh={refresh}
                    refreshing={refreshing}
                    keyExtractor={keyExtractor}
                    renderItem={promotionItem}
                    ItemSeparatorComponent={Separator}
                    onEndReached={onEndReached}
                    onEndReachedThreshold={0.33}
                    initialNumToRender={25}
                    ListFooterComponent={loader}
                />
            </View>
        </Container>
    );
}

export interface PromotionsProps {
    loading: boolean;
    refreshing: boolean;
    load: () => void;
    refresh: () => void;
    data: Store.Api.PagedResponse<Store.Promotion>;
}
