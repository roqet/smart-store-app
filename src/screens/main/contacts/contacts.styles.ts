import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    scroll: {
        position: 'relative'
    },
    content: {
        width: '100%',
        padding: 20
    },
    call: {
        width: 60,
        display: 'flex',
        justifyContent: 'center',
        position: 'absolute',
        right: 20,
        bottom: 20
    },
    callIcon: {
        color: '#fff'
    }
});

export const tags = StyleSheet.create({
    p: {
        fontSize: 18
    }
});
