import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    wrapper: {
        marginBottom: 14
    },
    note: {
        fontSize: 14,
        lineHeight: 17,
        marginBottom: 3
    },
    email: {
        fontSize: 24,
        lineHeight: 29,
        fontWeight: '900'
    }
});
