import React from 'react';
import { View, Text } from 'native-base';
import { i18n } from '@common/services';
import { styles } from './emails.styles';

interface Emial {
    label: string;
    email: string;
}

export function ContactsEmails({ meta: { hr, info } }: Store.Contacts): JSX.Element {
    const emails: Emial[] = [
        {
            label: `${i18n.t('forms.generalQuestions')}:`,
            email: info
        },
        {
            label: `${i18n.t('forms.employmentIssues')}:`,
            email: hr
        }
    ];

    return (
        <View>
            {emails.map(
                ({ label, email }): JSX.Element => {
                    return (
                        <View key={label} style={styles.wrapper}>
                            <Text style={styles.note}>{label}</Text>
                            <Text style={styles.email}>{email}</Text>
                        </View>
                    );
                }
            )}
        </View>
    );
}
