import React, { useEffect } from 'react';
import { Linking } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Container, Header, Content, Left, Button, Body, Title, Right, View } from 'native-base';
import { i18n } from '@common/services';
import { Icons } from '@common/constants';
import { Loader, Icon, Html } from '@common/components';
import { styles, tags } from './contacts.styles';
import { ContactsSocials } from './socials/socials.component';
import { ContactsEmails } from './emails/emails.component';

const call = (tel: string): void => {
    Linking.openURL(`tel:${tel.replace(/\s/g, '')}`).catch(console.log);
};

export function Contacts({ initialized, contacts, load, loading }: ContactsProps): JSX.Element {
    useEffect((): void => {
        if (!initialized) load();
    }, [initialized, load]);

    const loader: JSX.Element = !initialized && <Loader show={loading} size='large' />;

    const content: JSX.Element = initialized && contacts && (
        <>
            <View style={styles.content}>
                <Html content={contacts.content} styles={tags} />
                <ContactsSocials {...contacts} />
                <ContactsEmails {...contacts} />
            </View>
        </>
    );

    const caller: JSX.Element = initialized && (
        <Button
            iconLeft
            rounded
            large
            dark
            style={styles.call}
            onPress={(): void => call(contacts.meta.tel)}
        >
            <Icon name={Icons.call} style={styles.callIcon} />
        </Button>
    );

    return (
        <Container>
            <Header noShadow iosBarStyle='dark-content' translucent={false}>
                <Left style={{ flex: 0 }}>
                    <Button transparent onPress={(): void => Actions.pop()}>
                        <Icon name='chevron-left' />
                    </Button>
                </Left>
                <Body>
                    <Title>{i18n.t('pages.contacts')}</Title>
                </Body>
                <Right />
            </Header>
            <Content>
                {loader}
                {content}
            </Content>
            {caller}
        </Container>
    );
}

export interface ContactsProps {
    loading: boolean;
    initialized: boolean;
    contacts: Store.Contacts;
    load: () => void;
}
