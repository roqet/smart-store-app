import { connect } from 'react-redux';
import { Contacts, ContactsProps } from './contacts.component';
import { ContactsSelectors, ContactsActions } from '@/store/contacts';

const mapStateToProps = (state: AppState): Partial<ContactsProps> => ({
    loading: ContactsSelectors.loading(state),
    initialized: ContactsSelectors.initialized(state),
    contacts: ContactsSelectors.contacts(state)
});

const mapDispatchToProps = (dispatch): Partial<ContactsProps> => ({
    load: (): void => dispatch(new ContactsActions.LoadContacts())
});

export const ContactsContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Contacts as any);
