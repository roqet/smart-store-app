import React from 'react';
import { Linking } from 'react-native';
import { Button, View } from 'native-base';
import { Icons } from '@common/constants';
import { Icon } from '@common/components';
import { styles } from './socials.styles';

interface SocialBtn {
    icon: Icons;
    link: string;
}

const open = (link: string): void => {
    Linking.openURL(link).catch(console.log);
};

export function ContactsSocials({
    meta: { facebook, telegram, instagram }
}: Store.Contacts): JSX.Element {
    const btns: SocialBtn[] = [
        {
            icon: Icons.facebook,
            link: facebook
        },
        {
            icon: Icons.telegram,
            link: telegram
        },
        {
            icon: Icons.instagram,
            link: instagram
        }
    ];
    return (
        <View style={styles.icons}>
            {btns.map(
                ({ icon, link }): JSX.Element => {
                    return (
                        <Button
                            transparent
                            iconLeft
                            rounded
                            light
                            large
                            key={icon}
                            style={styles.btn}
                            onPress={(): void => open(link)}
                        >
                            <Icon style={styles.icon} name={icon} />
                        </Button>
                    );
                }
            )}
        </View>
    );
}
