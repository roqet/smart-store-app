import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    icons: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        padding: 10,
        marginBottom: 10
    },
    icon: {
        color: '#6C6C6C'
    },
    btn: {
        display: 'flex',
        justifyContent: 'center',
        width: 60,
        marginLeft: 6,
        marginRight: 6
    }
});
