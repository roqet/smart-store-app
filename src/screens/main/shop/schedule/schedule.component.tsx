import React from 'react';
import { Col, Row, Grid, Text, View } from 'native-base';
import { i18n } from '@common/services';
import { styles } from './schedule.styles';
import { ScheduleWeekday } from './weekday/weekday.component';

interface ShopScheduleProps {
    shop: Store.Shop;
}

export function ShopSchedule({
    shop: { workingSchedule, prices, deliveryLux, dayOfDelivery }
}: ShopScheduleProps): JSX.Element {
    const heading = (
        <Row style={styles.row}>
            <Col style={styles.headingWeekdayCol} />
            <Col style={styles.headingScheduleCol}>
                <Text style={styles.headingText}>{i18n.t('forms.schedule')}</Text>
            </Col>
            <Col style={styles.headingDeliveryCol}>
                <Text style={styles.headingText}>{i18n.t('forms.priceOfGoodsUahKg')}</Text>
            </Col>
            <Col style={styles.headingLuxCol}>
                <Text style={styles.headingText}>{i18n.t('forms.priceProductLuxuryUahKg')}</Text>
            </Col>
        </Row>
    );

    const pricing = (
        <>
            {workingSchedule.map(
                ({ dayNum, start, end }): JSX.Element => {
                    return (
                        <Row key={dayNum} style={styles.row}>
                            <Col style={styles.rowWeekdayCol}>
                                <ScheduleWeekday
                                    dayNum={dayNum}
                                    deliveryLux={deliveryLux}
                                    dayOfDelivery={dayOfDelivery}
                                />
                            </Col>
                            <Col style={styles.rowScheduleCol}>
                                <Text style={styles.colText}>
                                    {i18n.t('forms.scheduleStartEnd', { start, end })}
                                </Text>
                            </Col>
                            <Col style={styles.rowDeliveryCol}>
                                <Text style={styles.colText}>{prices.deliveryPrices[dayNum]}</Text>
                            </Col>
                            <Col style={styles.rowLuxCol}>
                                <Text style={styles.colText}>{prices.luxuryPrices[dayNum]}</Text>
                            </Col>
                        </Row>
                    );
                }
            )}
        </>
    );

    return (
        <Grid style={styles.grid}>
            {heading}
            {pricing}
        </Grid>
    );
}
