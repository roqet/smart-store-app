import StyleSheet from 'react-native-extended-stylesheet';

const col = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
};

const headingCol = {
    display: 'flex',
    alignItems: 'center'
};

export const styles = StyleSheet.create({
    grid: {
        paddingBottom: 20
    },
    row: {
        paddingBottom: 20
    },
    rowWeekdayCol: {
        ...col,
        alignItems: 'flex-start',
        width: 66
    },
    rowScheduleCol: {
        ...col
    },
    rowDeliveryCol: {
        ...col
    },
    rowLuxCol: {
        ...col,
        width: 70
    },
    headingWeekdayCol: {
        ...headingCol,
        width: 66
    },
    headingScheduleCol: {
        ...headingCol
    },
    headingDeliveryCol: {
        ...headingCol
    },
    headingLuxCol: {
        ...headingCol,
        width: 70
    },
    headingText: {
        color: '$dustyGray',
        fontSize: 12,
        lineHeight: 14,
        textAlign: 'center'
    },
    colText: {
        fontFamily: 'Roboto',
        fontSize: 17,
        lineHeight: 21
    }
});
