import StyleSheet from 'react-native-extended-stylesheet';

const weekday = {
    width: 56,
    height: 32,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
};

const text = {
    fontSize: 14,
    lineHeight: 16,
    fontWeight: 'bold',
    fontFamily: 'Roboto'
};

export const styles = StyleSheet.create({
    default: {
        ...weekday,
        borderWidth: 1,
        borderColor: '$dustyGray'
    },
    delivery: {
        ...weekday,
        backgroundColor: '$black'
    },
    lux: {
        ...weekday,
        backgroundColor: '$primary'
    },
    defaultText: {
        ...text
    },
    deliveryText: {
        ...text,
        color: '$white'
    }
});
