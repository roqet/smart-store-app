import React from 'react';
import { View, Text } from 'native-base';
import { weekdays } from '@common/utils';
import { styles } from './weekday.styles';

interface ScheduleWeekdayProps {
    dayNum: number;
    dayOfDelivery: number;
    deliveryLux: number;
}

export function ScheduleWeekday({
    dayNum,
    dayOfDelivery,
    deliveryLux
}: ScheduleWeekdayProps): JSX.Element {
    const weekday = ((): object => {
        if (dayNum === dayOfDelivery) return styles.delivery;
        if (dayNum === deliveryLux) return styles.lux;
        return styles.default;
    })();

    const text = ((): object => {
        if (dayNum === dayOfDelivery) return styles.deliveryText;
        return styles.defaultText;
    })();

    return (
        <View style={weekday}>
            <Text style={text}>{weekdays[dayNum]}</Text>
        </View>
    );
}
