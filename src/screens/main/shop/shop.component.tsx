/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, EffectCallback } from 'react';
import { Actions } from 'react-native-router-flux';
import { Container, Left, Header, Button, Body, Title, Right, Content, View } from 'native-base';
import { i18n } from '@common/services';
import { Icon, Legend, Loader } from '@common/components';
import { styles } from './shop.styles';
import { ShopTitle } from './title/title.component';
import { ShopSchedule } from './schedule/schedule.component';
import { ShopMapBtn } from './map-btn/map.btn.component';

export interface ShopProps {
    id: number;
    loading: boolean;
    shop: Store.Shop;
    cities: Store.City[];
    load: (id: number) => void;
}

export function Shop({ loading, shop, id, load, cities }: ShopProps): JSX.Element {
    useEffect((): void => {
        if (!shop || shop.id !== id) load(id);
    }, [load]);

    const loader: JSX.Element = loading && <Loader show={loading} size='large' />;

    const content = !loading && shop && (
        <View style={styles.content}>
            <ShopTitle shop={shop} cities={cities} />
            <ShopSchedule shop={shop} />
            <Legend />
            <ShopMapBtn shop={shop} />
        </View>
    );

    return (
        <Container>
            <Header noShadow iosBarStyle='dark-content' translucent={false}>
                <Left style={{ flex: 0 }}>
                    <Button transparent onPress={(): void => Actions.pop()}>
                        <Icon name='chevron-left' />
                    </Button>
                </Left>
                <Body>
                    <Title>{i18n.t('pages.shops')}</Title>
                </Body>
                <Right />
            </Header>
            <Content>
                {loader}
                {content}
            </Content>
        </Container>
    );
}
