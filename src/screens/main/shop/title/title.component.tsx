import React from 'react';
import { View, Text } from 'native-base';
import { i18n } from '@common/services';
import { styles } from './title.style';

interface ShopTitleProps {
    shop: Store.Shop;
    cities: Store.City[];
}

export function ShopTitle({ shop, cities }: ShopTitleProps): JSX.Element {
    return (
        <View style={styles.container}>
            <Text style={styles.address}>{shop.address.name}</Text>
            <Text style={styles.city}>
                {i18n.t('forms.cDotName', {
                    name: (
                        cities.find(({ id }): boolean => {
                            return Number(id) === Number(shop.address.cityId);
                        }) || { name: '' }
                    ).name
                })}
            </Text>
        </View>
    );
}
