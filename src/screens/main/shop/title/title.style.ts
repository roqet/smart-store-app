import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    container: {
        paddingBottom: 30
    },
    address: {
        fontSize: 24,
        lineHeight: 24,
        textAlign: 'center',
        marginBottom: 14
    },
    city: {
        color: '$dustyGray',
        textAlign: 'center',
        fontSize: 14,
        lineHeight: 22
    }
});
