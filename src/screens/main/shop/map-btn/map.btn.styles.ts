import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    container: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        paddingTop: 45
    },
    button: {
        borderRadius: 0,
        width: 166
    },
    text: {
        fontSize: 14,
        lineHeight: 16
    }
});
