import React from 'react';
import { Actions } from 'react-native-router-flux';
import { View, Button, Text } from 'native-base';
import { i18n } from '@common/services';
import { styles } from './map.btn.styles';

interface ShopMapBtnProps {
    shop: Store.Shop;
}

export function ShopMapBtn({ shop }: ShopMapBtnProps): JSX.Element {
    return (
        <View style={styles.container}>
            <Button
                dark
                bordered
                style={styles.button}
                onPress={(): void => Actions.push('modal.map', { shop })}
            >
                <Text style={styles.text}>{i18n.t('buttons.shopOnMap').toUpperCase()}</Text>
            </Button>
        </View>
    );
}
