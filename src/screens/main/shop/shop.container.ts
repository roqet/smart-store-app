import { connect } from 'react-redux';
import { Shop, ShopProps } from './shop.component';
import { ShopSelectors, ShopActions } from '@/store/shop';
import { ShopsSelectors } from '@/store/shops';

const mapStateToProps = (state: AppState, props: ShopProps): Partial<ShopProps> => ({
    id: props.id,
    loading: ShopSelectors.loading(state),
    shop: ShopSelectors.shop(state),
    cities: ShopsSelectors.cities(state)
});

const mapDispatchToProps = (dispatch): Partial<ShopProps> => ({
    load: (id: number): void => dispatch(new ShopActions.LoadShop(id))
});

export const ShopContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Shop);
