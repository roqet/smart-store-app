import { connect } from 'react-redux';
import { AuthActions, AuthSelectors } from '@/store/auth';
import { AuthRegStep2, AuthRegStep2Props } from './reg-step-2.component';

const mapStateToProps = (state: AppState): Partial<AuthRegStep2Props> => ({
    loading: AuthSelectors.loading(state)
});

const mapDispatchToProps = (dispatch): Partial<AuthRegStep2Props> => ({
    createUser: (data): void => dispatch(new AuthActions.CreateUser(data))
});

export const AuthRegStep2Container = connect(
    mapStateToProps,
    mapDispatchToProps
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
)(AuthRegStep2 as any);
