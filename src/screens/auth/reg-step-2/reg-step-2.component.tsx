/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable no-console */
import env from 'react-native-config';
import React, { useState } from 'react';
import { TouchableOpacity, Linking } from 'react-native';
import { Text } from 'native-base';
import { Formik } from 'formik';
import * as Yup from 'yup';
import { i18n } from '@common/services';
import { Form } from '@common/components';
import { Auth } from '../auth.component';
import { styles } from './reg-step-2.styles';

const validationSchema = Yup.object().shape({
    email: Yup.string().email()
});

export function AuthRegStep2({ loading, createUser }: AuthRegStep2Props): JSX.Element {
    const [policy, setPolicy] = useState<boolean>(false);
    const [data, setData] = useState<Partial<Store.User>>({});

    return (
        <Auth
            loading={loading}
            valid
            title={i18n.t('forms.registration')}
            note={i18n.t('forms.step2')}
            style={{ paddingTop: 27 }}
            currentStep='auth.regStep2'
            nextStep={null}
        >
            <Formik
                initialValues={{
                    email: ''
                }}
                isInitialValid
                validationSchema={validationSchema}
                onSubmit={(): void => {}}
            >
                {({ handleChange, values, isValid }): JSX.Element => {
                    setData(values);
                    return (
                        <Form title={i18n.t('forms.loginInformation')}>
                            <Form.Input
                                value={values.email}
                                type='email-address'
                                label={i18n.t('forms.emailOptional')}
                                onChange={handleChange('email')}
                            />
                            <Form.Checkbox value={policy} onChange={setPolicy}>
                                <TouchableOpacity
                                    onPress={(): void => {
                                        Linking.openURL((env as any).POLICY_URL).catch(console.log);
                                    }}
                                >
                                    <Text style={styles.policy}>{i18n.t('forms.policy')}</Text>
                                </TouchableOpacity>
                            </Form.Checkbox>
                            <Form.Button
                                label={i18n.t('buttons.complete')}
                                disabled={!(isValid && policy)}
                                onPress={(): void => createUser(data)}
                            />
                        </Form>
                    );
                }}
            </Formik>
        </Auth>
    );
}

export interface AuthRegStep2Props {
    loading: boolean;
    createUser: (data: Partial<Store.User>) => void;
}
