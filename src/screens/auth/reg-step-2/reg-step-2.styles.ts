import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    policy: {
        fontSize: 14,
        fontWeight: 'bold',
        textDecorationLine: 'underline',
        lineHeight: 22
    }
});
