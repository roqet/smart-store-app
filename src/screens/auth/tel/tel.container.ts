import { connect } from 'react-redux';
import { AuthTel, AuthTelProps } from './tel.component';
import { AuthActions, AuthSelectors } from '@/store/auth';

const mapStateToProps = (state: AppState): Partial<AuthTelProps> => ({
    loading: AuthSelectors.loading(state),
    step: AuthSelectors.step(state)
});

const mapDispatchToProps = (dispatch): Partial<AuthTelProps> => ({
    sendPassword: (tel: string): void => dispatch(new AuthActions.SendPassword(tel))
});

export const AuthTelContainer = connect(
    mapStateToProps,
    mapDispatchToProps
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
)(AuthTel as any);
