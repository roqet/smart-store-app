import React from 'react';
import { withFormik, InjectedFormikProps } from 'formik';
import * as Yup from 'yup';
import { i18n } from '@common/services';
import { Form } from '@common/components';
import { AuthSteps } from '@/store/auth';
import { Auth } from '../auth.component';

interface FormValues {
    tel: string;
}

function AuthTelForm({
    sendPassword,
    step,
    loading,
    values,
    handleChange,
    isValid
}: InjectedFormikProps<AuthTelProps, FormValues>): JSX.Element {
    return (
        <Auth
            valid={isValid}
            loading={loading}
            onNext={(): void => sendPassword(values.tel)}
            style={{ paddingTop: 53 }}
            currentStep='auth.tel'
            nextStep={step}
        >
            <Form title={i18n.t('labels.welcome')} spacing={53}>
                <Form.Input
                    value={values.tel}
                    size='L'
                    type='phone-pad'
                    label={i18n.t('forms.enterYourPhoneNumber')}
                    onChange={handleChange('tel')}
                />
            </Form>
        </Auth>
    );
}

export const AuthTel = withFormik<AuthTelProps, FormValues>({
    mapPropsToValues: (): FormValues => ({ tel: '+380' }),
    handleSubmit: (): void => {},
    validateOnChange: true,
    validationSchema: Yup.object().shape({
        tel: Yup.string()
            .required()
            .matches(
                /(^\+[0-9]{2}|^\+[0-9]{2}\(0\)|^\(\+[0-9]{2}\)\(0\)|^00[0-9]{2}|^0)([0-9]{9}$|[0-9\-\s]{10}$)/
            )
    })
})(AuthTelForm);

export interface AuthTelProps {
    loading: boolean;
    step: AuthSteps;
    sendPassword: (tel: string) => void;
}
