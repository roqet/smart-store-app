import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    timer: {
        fontSize: 20,
        textAlign: 'center'
    }
});
