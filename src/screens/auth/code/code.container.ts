import { connect } from 'react-redux';
import { AuthCode, AuthCodeProps } from './code.component';
import { AuthActions, AuthSelectors } from '@/store/auth';

const mapStateToProps = (state: AppState): Partial<AuthCodeProps> => ({
    data: AuthSelectors.data(state),
    loading: AuthSelectors.loading(state),
    step: AuthSelectors.step(state)
});

const mapDispatchToProps = (dispatch): Partial<AuthCodeProps> => ({
    login: (code): void => dispatch(new AuthActions.Login(code))
});

export const AuthCodeContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthCode);
