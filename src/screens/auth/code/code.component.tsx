import get from 'lodash/get';
import React, { useState, useEffect } from 'react';
import { Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { withFormik, FormikValues, InjectedFormikProps } from 'formik';
import * as Yup from 'yup';
import { Icons } from '@common/constants';
import { i18n } from '@common/services';
import { Form } from '@common/components';
import { AuthSteps, AuthData } from '@/store/auth/auth.reducer';
import { Auth } from '../auth.component';
import { styles } from './code.styles';

interface FormValues extends FormikValues {
    tel: string;
    code: string;
}

function AuthCodeForm({
    loading,
    login,
    step,
    handleChange,
    values,
    isValid
}: InjectedFormikProps<AuthCodeProps, FormValues>): JSX.Element {
    const [timer, setTimer] = useState(59);

    useEffect((): void => {
        if (timer !== 0) {
            setTimeout((): void => {
                setTimer(timer - 1);
            }, 1000);
        }
    }, [timer]);

    return (
        <Auth
            loading={loading}
            onNext={(): void => login(values.code)}
            valid={isValid}
            style={{ paddingTop: 53 }}
            currentStep='auth.code'
            nextStep={step}
        >
            <Form spacing={53}>
                <Form.Input
                    value={values.tel}
                    size='L'
                    type='default'
                    disabled
                    label={i18n.t('forms.yourPhoneNumber')}
                    next={
                        <Form.Input.Action
                            icon={Icons.edit}
                            onPress={(): void => Actions.replace('auth.tel')}
                        />
                    }
                />
                <Form.Input
                    value={values.code}
                    size='L'
                    type='numeric'
                    label={i18n.t('forms.codeFromSMS')}
                    onChange={handleChange('code')}
                />
            </Form>
            <Text style={styles.timer}>{i18n.t('forms.timerIn', { in: `00:${timer}` })}</Text>
        </Auth>
    );
}

export interface AuthCodeProps {
    step: AuthSteps;
    data: AuthData;
    loading: boolean;
    login: (code: string) => void;
}

export const AuthCode = withFormik<AuthCodeProps, FormValues>({
    mapPropsToValues: ({ data }): FormValues => ({ tel: get(data, 'phoneNumber', ''), code: '' }),
    handleSubmit: (): void => {},
    validateOnChange: true,
    validationSchema: Yup.object().shape({
        code: Yup.number()
            .required()
            .integer()
            .test('len', 'Must be exactly 4 characters', (val = 0): boolean => {
                return val.toString().length === 4;
            })
    })
})(AuthCodeForm);
