import StyleSheet from 'react-native-extended-stylesheet';

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '$primary'
    },
    content: {
        flex: 1,
        paddingLeft: 47,
        paddingRight: 47,
        paddingTop: 56,
        paddingBottom: 56
    },
    logo: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center'
    },
    image: {
        width: 116,
        height: 63
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 26
    },
    note: {
        fontSize: 16,
        fontWeight: 'bold',
        textAlign: 'center',
        marginBottom: 40
    },
    footer: {
        height: 68,
        backgroundColor: '$white'
    },
    button: {
        flex: 1
    },
    text: {
        fontSize: 20,
        lineHeight: 22
    },
    loader: {
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        width: '100%',
        height: '100%'
    }
});
