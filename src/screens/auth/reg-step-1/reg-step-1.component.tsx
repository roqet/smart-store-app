import React from 'react';
import { withFormik, InjectedFormikProps } from 'formik';
import * as Yup from 'yup';
import { i18n } from '@common/services';
import { Form } from '@common/components';
import { AuthSteps } from '@/store/auth';
import { Auth } from '../auth.component';

interface FormValues {
    firstName: string;
    lastName: string;
    dateOfBirth: number;
    sex: Store.Sex;
}

export function AuthRegStep1Form({
    updateData,
    step,
    next,
    isValid,
    values,
    handleChange
}: InjectedFormikProps<AuthRegStep1Props, FormValues>): JSX.Element {
    const onNext = (): void => {
        updateData(values);
        next('auth.card');
    };

    return (
        <Auth
            loading={false}
            title={i18n.t('forms.registration')}
            note={i18n.t('forms.step1')}
            onNext={onNext}
            valid={isValid}
            style={{ paddingTop: 27 }}
            currentStep='auth.regStep1'
            nextStep={step}
        >
            <Form title={i18n.t('forms.personalData')}>
                <Form.Input
                    value={values.lastName}
                    type='default'
                    label={i18n.t('forms.lastName')}
                    onChange={handleChange('lastName')}
                />
                <Form.Input
                    value={values.firstName}
                    type='default'
                    label={i18n.t('forms.firstName')}
                    onChange={handleChange('firstName')}
                />
                <Form.Datepicker
                    value={values.dateOfBirth}
                    type='default'
                    label={i18n.t('forms.dateOfBirth')}
                    onChange={handleChange('dateOfBirth')}
                />
                <Form.RadioGroup>
                    <Form.Radio
                        value={values.sex}
                        label={i18n.t('forms.male')}
                        selected={0}
                        onChange={handleChange('sex')}
                    />
                    <Form.Radio
                        value={values.sex}
                        label={i18n.t('forms.female')}
                        selected={1}
                        onChange={handleChange('sex')}
                    />
                </Form.RadioGroup>
            </Form>
        </Auth>
    );
}

export interface AuthRegStep1Props {
    step: AuthSteps;
    next: (step: AuthSteps) => void;
    updateData: (data: Partial<Store.User>) => void;
}

export const AuthRegStep1 = withFormik<AuthRegStep1Props, FormValues>({
    validateOnChange: true,
    mapPropsToValues: (): FormValues => ({
        firstName: '',
        lastName: '',
        dateOfBirth: undefined,
        sex: undefined
    }),
    validationSchema: Yup.object().shape({
        firstName: Yup.string().required(),
        lastName: Yup.string().required(),
        dateOfBirth: Yup.number().required(),
        sex: Yup.number().required()
    }),
    handleSubmit: (): void => {}
})(AuthRegStep1Form);
