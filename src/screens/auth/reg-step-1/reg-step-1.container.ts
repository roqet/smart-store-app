import { connect } from 'react-redux';
import { AuthActions, AuthSelectors } from '@/store/auth';
import { AuthRegStep1, AuthRegStep1Props } from './reg-step-1.component';

const mapStateToProps = (state: AppState): Partial<AuthRegStep1Props> => ({
    step: AuthSelectors.step(state)
});

const mapDispatchToProps = (dispatch): Partial<AuthRegStep1Props> => ({
    updateData: (data): void => dispatch(new AuthActions.UpdateData(data)),
    next: (step): void => dispatch(new AuthActions.Next(step))
});

export const AuthRegStep1Container = connect(
    mapStateToProps,
    mapDispatchToProps
)(AuthRegStep1);
