import React, { PropsWithChildren, useEffect } from 'react';
import { Image, ScrollView, ViewStyle } from 'react-native';
import { Button, View, Text, Container } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { i18n } from '@common/services';
import { Loader } from '@common/components';
import { AuthSteps } from '@/store/auth';
import { styles } from './auth.styles';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const logo = require('../../assets/logo.png');

interface AuthProps {
    loading: boolean;
    valid: boolean;
    style?: ViewStyle;
    title?: string;
    note?: string;
    onNext?: () => void;
    nextStep: AuthSteps;
    currentStep: AuthSteps;
}

export function Auth({
    onNext,
    currentStep,
    nextStep,
    children,
    valid,
    title,
    note,
    loading,
    style = {}
}: PropsWithChildren<AuthProps>): JSX.Element {
    useEffect((): void => {
        if (nextStep && Actions.currentScene === currentStep && nextStep !== currentStep) {
            Actions.push(nextStep);
        }
    }, [currentStep, nextStep]);

    const titleEl = title && <Text style={styles.title}>{title}</Text>;

    const noteEl = note && <Text style={styles.note}>{note}</Text>;

    const loaderEl = loading && (
        <View style={styles.loader}>
            <Loader show size='large' />
        </View>
    );

    const footerEl = onNext && (
        <View style={styles.footer}>
            <Button
                full
                light
                transparent
                style={styles.button}
                disabled={!valid || loading}
                onPress={onNext}
            >
                <Text style={styles.text}>{i18n.t('buttons.proceed').toUpperCase()}</Text>
            </Button>
        </View>
    );

    return (
        <Container>
            <View style={styles.container}>
                <ScrollView style={styles.content}>
                    <View style={styles.logo}>
                        <Image style={styles.image} source={logo} />
                    </View>
                    <View style={style}>
                        {titleEl}
                        {noteEl}
                        {children}
                    </View>
                </ScrollView>
                {footerEl}
            </View>
            {loaderEl}
        </Container>
    );
}
