import React, { useState } from 'react';
import { Formik } from 'formik';
import { i18n } from '@common/services';
import { Form } from '@common/components';
import { AuthSteps } from '@/store/auth';
import { Auth } from '../auth.component';

const validate = (values, hasCard): object => {
    const { length } = String(values.cardNumber || '');
    if (hasCard && [13, 16].indexOf(length) === -1) return { required: 'Invalid' };
    return {};
};

export function AuthRegCrd({ step, updateData, next }: AuthRegCrdProps): JSX.Element {
    const [valid, setValid] = useState<boolean>(true);
    const [hasCard, setHasCard] = useState(false);
    const [data, setData] = useState<Partial<Store.User>>({});

    const onNext = (): void => {
        updateData(data);
        next('auth.regStep2');
    };

    return (
        <Auth
            loading={false}
            valid={valid}
            onNext={hasCard ? onNext : undefined}
            style={{ paddingTop: 117 }}
            currentStep='auth.card'
            nextStep={step}
        >
            <Formik
                initialValues={{
                    cardNumber: ''
                }}
                validate={(values): object => validate(values, hasCard)}
                onSubmit={(): void => {}}
            >
                {({ handleChange, values, isValid }): JSX.Element => {
                    setValid(isValid);
                    setData(values);
                    return (
                        <Form>
                            <Form.Button
                                theme='light'
                                label={i18n.t('buttons.iDoNotHaveCard')}
                                onPress={(): void => {
                                    next('auth.regStep2');
                                }}
                            />
                            <Form.Button
                                theme='dark'
                                label={i18n.t('buttons.iHaveCard')}
                                onPress={(): void => {
                                    setHasCard(true);
                                }}
                            />
                            {hasCard && (
                                <Form.Input
                                    size='L'
                                    type='numeric'
                                    value={values.cardNumber}
                                    onChange={handleChange('cardNumber')}
                                    label={i18n.t('forms.enterTheSerialNumber')}
                                    note={i18n.t('notes.enterTheSerialNumber')}
                                />
                            )}
                        </Form>
                    );
                }}
            </Formik>
        </Auth>
    );
}

export interface AuthRegCrdProps {
    step: AuthSteps;
    next: (step: AuthSteps) => void;
    updateData: (data: Partial<Store.User>) => void;
}
