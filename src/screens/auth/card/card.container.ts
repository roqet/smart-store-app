import { connect } from 'react-redux';
import { AuthActions, AuthSelectors } from '@/store/auth';
import { AuthRegCrd, AuthRegCrdProps } from './card.component';

const mapStateToProps = (state: AppState): Partial<AuthRegCrdProps> => ({
    step: AuthSelectors.step(state)
});

const mapDispatchToProps = (dispatch): Partial<AuthRegCrdProps> => ({
    updateData: (data): void => dispatch(new AuthActions.UpdateData(data)),
    next: (step): void => dispatch(new AuthActions.Next(step))
});

export const AuthRegCrdContainer = connect(
    mapStateToProps,
    mapDispatchToProps
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
)(AuthRegCrd as any);
