/* eslint-disable @typescript-eslint/no-explicit-any */
import { connect } from 'react-redux';
import { Splash } from './splash.component';

export const SplashContainer = connect()(Splash as any);
