import React, { PropsWithChildren, useEffect } from 'react';
import { Toast } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { useTranslation } from 'react-i18next';
import SplashScreen from 'react-native-splash-screen';
import Orientation, { OrientationType } from 'react-native-orientation-locker';

type ScreenOrientation = 'LANDSCAPE' | 'PORTRAIT';

export interface ScreensProps {
    user: Store.User;
    errors: string[];
    initialized: boolean;
    init: () => void;
}

export function Screens({
    children,
    init,
    initialized,
    user,
    errors
}: PropsWithChildren<ScreensProps>): JSX.Element {
    const { t } = useTranslation();

    Orientation.lockToPortrait();

    useEffect((): void => {
        Orientation.addDeviceOrientationListener((orientation: OrientationType): void => {
            const isVertical = orientation === 'PORTRAIT' || orientation === 'PORTRAIT-UPSIDEDOWN';
            const isMain = Actions.currentScene.indexOf('main') !== -1;
            const isCard = Actions.currentScene === 'modal.card';
            if (!isVertical && isMain && !isCard) {
                Actions.push('modal.card');
            }
        });
    }, []);

    useEffect((): void => {
        if (errors.length) {
            errors.forEach((error): void => {
                Toast.show({
                    text: t(error),
                    type: 'danger',
                    duration: 3000
                });
            });
        }
    }, [errors, errors.length, t]);

    useEffect((): void => {
        if (initialized && user) {
            Actions.reset('main');
            SplashScreen.hide();
        } else if (initialized && !user) {
            Actions.reset('auth');
            SplashScreen.hide();
        } else {
            init();
        }
    }, [init, initialized, user]);

    return <>{children}</>;
}
