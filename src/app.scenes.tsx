import React from 'react';
import { Actions, Scene, Modal } from 'react-native-router-flux';

import { SplashContainer } from '@/screens/splash/splash.container';
import { HomeContainer } from '@/screens/main/home/home.container';
import { PromotionsContainer } from '@/screens/main/promotions/promotions.container';
import { PromotionContainer } from '@/screens/main/promotion/promotion.container';
import { ContactsContainer } from './screens/main/contacts/contacts.container';
import { AboutContainer } from '@/screens/main/about/about.container';
import { ShopsContainer } from '@/screens/main/shops/shops.container';
import { ShopContainer } from '@/screens/main/shop/shop.container';
import { ProfileContainer } from '@/screens/main/profile/profile.container';
import { NotificationsContainer } from '@/screens/main/notifications/notifications.container';

import { AuthTelContainer } from '@/screens/auth/tel/tel.container';
import { AuthCodeContainer } from '@/screens/auth/code/code.container';
import { AuthRegStep1Container } from '@/screens/auth/reg-step-1/reg-step-1.container';
import { AuthRegStep2Container } from '@/screens/auth/reg-step-2/reg-step-2.container';
import { AuthRegCrdContainer } from '@/screens/auth/card/card.container';

import { CitiesContainer } from '@/modals/cities/cities.container';
import { MapContainer } from '@/modals/map/map.container';
import { LevelsContainer } from '@/modals/levels/levels.container';
import { CardContainer } from '@/modals/card/card.container';

export const Scenes = Actions.create(
    <Modal hideNavBar>
        <Scene hideNavBar key='splash' component={SplashContainer} />
        <Scene key='auth' hideTabBar hideNavBar>
            <Scene hideNavBar key='auth.tel' component={AuthTelContainer} />
            <Scene hideNavBar key='auth.code' component={AuthCodeContainer} />
            <Scene hideNavBar initial key='auth.regStep1' component={AuthRegStep1Container} />
            <Scene hideNavBar key='auth.regStep2' component={AuthRegStep2Container} />
            <Scene hideNavBar key='auth.card' component={AuthRegCrdContainer} />
        </Scene>
        <Scene key='main' hideTabBar hideNavBar showLabel={false} lazy={false}>
            <Scene hideNavBar key='main.home' component={HomeContainer} />
            <Scene hideNavBar key='main.promotions' component={PromotionsContainer} />
            <Scene hideNavBar key='main.promotion' component={PromotionContainer} />
            <Scene hideNavBar key='main.contacts' component={ContactsContainer} />
            <Scene hideNavBar key='main.about' component={AboutContainer} />
            <Scene hideNavBar key='main.shops' component={ShopsContainer} />
            <Scene hideNavBar key='main.shop' component={ShopContainer} />
            <Scene hideNavBar key='main.profile' component={ProfileContainer} />
            <Scene hideNavBar key='main.notifications' component={NotificationsContainer} />
        </Scene>
        <Scene hideNavBar key='cities' component={CitiesContainer} />
        <Scene hideNavBar key='modal.map' component={MapContainer} />
        <Scene hideNavBar key='modal.levels' component={LevelsContainer} />
        <Scene hideNavBar key='modal.card' component={CardContainer} />
    </Modal>
);
