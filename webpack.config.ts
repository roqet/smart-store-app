import * as path from 'path';
import { Configuration } from 'webpack';
import Merge from 'webpack-merge';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import TerserPlugin from 'terser-webpack-plugin';
import GenerateJsonPlugin from 'generate-json-webpack-plugin';
import * as locales from './assets/src/locales';

const config: Configuration = {
    entry: {
        assets: ['./assets/src/icons/icon.font.js', './assets/src/fonts/fonts.ts']
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    mode: process.env.NODE_ENV as 'development' | 'production' | 'none',
    devtool: 'cheap-module-source-map',
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                use: [
                    {
                        loader: 'ts-loader'
                    }
                ]
            },
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader']
                })
            },
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'sass-loader']
                })
            },
            {
                test: /\.(eot|svg|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: 'fonts/[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 1
                        }
                    }
                ]
            },
            {
                test: /\.font\.(js|json)$/,
                loader: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: { url: false }
                        },
                        {
                            loader: 'webfonts-loader',
                            options: {
                                options: { publicPath: './' }
                            }
                        }
                    ]
                })
            }
        ]
    },
    plugins: [
        new ExtractTextPlugin('styles/[name].styles.css'),
        new TerserPlugin(),
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        ...Object.keys(locales).map((key): any => {
            return new GenerateJsonPlugin(`locales/${key}.json`, locales[key]);
        })
    ]
};

const common: Configuration = Merge(config, {
    output: {
        path: path.resolve(__dirname, './assets/dist'),
        filename: '[name].bundle.js'
    }
});

// const android: Configuration = Merge(config, {
//     output: {
//         path: path.resolve(__dirname, './android/app/src/main/assets'),
//         filename: '[name].bundle.js'
//     }
// });

export default [common];
