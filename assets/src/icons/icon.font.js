module.exports = {
    files: ['./*.svg'],
    fontName: 'SmartStore',
    classPrefix: 'si-',
    baseSelector: '.si',
    types: ['ttf'],
    fileName: 'fonts/[fontname].[ext]'
};
